::##############################################################################
::##############################################################################
::# (16.)TRANSFERTS ENTRE BRANCHES GIT LOCALES :
::##############################################################################
::##############################################################################


::##############################################################################
::# (16.01.)TRANSFERER, VERS UNE BRANCHE LOCALE, LE CONTENU D'UNE AUTRE BRANCHE LOCALE :
::##############################################################################
::# -->BRANCHE DE DESTINATION : LA BRANCHE LOCALE, SUR LAQUELLE ON DOIT POINTER PREALABLEMENT. 
::#                             ('git checkout ...').
::# -->BRANCHE D'ORIGINE      : LA BRANCHE LOCALE, SPECIFIEE DANS LA COMMANDE DE TRANSFERT.
::#                             ('git merge ...').
::# -->TYPE DE TRANSFERT      : TRANSFERT AVEC FUSION.
::##############################################################################
::# -->SYNTAXE   : git checkout [Nom de la branche locale de destination]
::# -->SYNTAXE   : git merge [Nom de la branche locale d'origine]
::##############################################################################
::# -->ATTENTION : LES INSTRUCTIONS CI-DESSOUS DOIVENT ETRE EXECUTEES DANS L'ORDRE !
::##############################################################################
git checkout dev
git merge master


::##############################################################################
::# (16.02.)TRANSFERER, VERS UNE BRANCHE LOCALE, LA REPLIQUE LOCALE D'UNE BRANCHE DISTANTE :
::##############################################################################
::# -->BRANCHE DE DESTINATION : LA BRANCHE LOCALE, SUR LAQUELLE ON DOIT POINTER PREALABLEMENT.
::#                             ('git checkout ...').
::# -->BRANCHE D'ORIGINE      : LA BRANCHE DISTANTE, SPECIFIEE DANS LA COMMANDE DE TRANSFERT.
::#                             ('git merge ...').
::# -->TYPE DE TRANSFERT      : TRANSFERT AVEC FUSION.
::##############################################################################
::# -->SYNTAXE : git checkout [Nom de la branche locale de destination]
::# -->SYNTAXE : git merge [Nom du dépôt distant d'origine]/[nom de la branche distante d'origine]
::##############################################################################
::# -->ATTENTION : LES INSTRUCTIONS CI-DESSOUS DOIVENT ETRE EXECUTEES DANS L'ORDRE !
::##############################################################################
git checkout master
git merge 25-Applications-WebService-MonoModule/master
