::##############################################################################
::##############################################################################
::# (14.)OPERATIONS SUR LE REPERTOIRE DE TRAVAIL ET LE PANIER LOCAL :
::##############################################################################
::##############################################################################


::##############################################################################
::# (14.01.)CONSULTER L'ETAT D'UNE BRANCHE LOCALE :
::##############################################################################
::# -->BRANCHE A CONSULTER : LA BRANCHE GIT LOCALE COURANTE
::##############################################################################
git status


::##############################################################################
::# (14.02.)ENREGISTRER, DANS LE PANIER LOCAL, LA MODIFICATION D'UN ELEMENT :
::##############################################################################
::# -->ELEMENT CONCERNE : TOUT ELEMENT (FICHIER OU REPERTOIRE) DU REPERTOIRE DE TRAVAIL AYANT SUBI UNE MODIFICATION.
::##############################################################################
git add [NOM DU FICHIER   ]
git add [NOM DU REPERTOIRE]


::##############################################################################
::# (14.03.)DESENREGISTRER, DANS LE PANIER LOCAL, LA MODIFICATION D'UN ELEMENT :
::##############################################################################
::# -->ELEMENT CONCERNE : TOUT ELEMENT (FICHIER OU REPERTOIRE) DU REPERTOIRE DE TRAVAIL AYANT SUBI UNE MODIFICATION.
::##############################################################################
git rm [NOM DU FICHIER   ]
git rm [NOM DU REPERTOIRE]
