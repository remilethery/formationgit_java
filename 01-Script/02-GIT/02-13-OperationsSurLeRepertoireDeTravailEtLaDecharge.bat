::##############################################################################
::##############################################################################
::# (13.)OPERATIONS SUR LE REPERTOIRE DE TRAVAIL ET LA "DECHARGE" :
::##############################################################################
::##############################################################################


::##############################################################################
::# (13.01.)CONSULTER L'ETAT D'UNE BRANCHE LOCALE :
::##############################################################################
::# -->BRANCHE A CONSULTER : LA BRANCHE GIT LOCALE COURANTE
::##############################################################################
git status


::##############################################################################
::# (13.02.)SAUVEGARDER, DANS LA "DECHARGE", LES MODIFICATIONS DU REPERTOIRE DE TRAVAIL :
::##############################################################################
git stash


::##############################################################################
::# (13.03.)AFFICHER LE CONTENU DE LA "DECHARGE" :
::##############################################################################
git stash list


::##############################################################################
::# (13.04.)SUPPRIMER LE CONTENU DE LA "DECHARGE" :
::##############################################################################
git stash drop
