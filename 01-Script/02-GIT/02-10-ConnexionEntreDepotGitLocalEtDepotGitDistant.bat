::##############################################################################
::##############################################################################
::# (10.)CONNEXION ENTRE DEPÔT GIT LOCAL ET DEPÔT GIT DISTANT :
::##############################################################################
::##############################################################################


::##############################################################################
::# (10.01.)AFFICHER LA LISTE DES DEPÔTS DISTANTS CONNECTES AU DEPÔT LOCAL COURANT :
::##############################################################################
::#      -->DEPÔTS DISTANTS :     LES DEPÔTS DISTANTS CONNECTES AU DEPÔT LOCAL COURANT.
::#      -->DEPÔT LOCAL COURANT : LE DEPÔT LOCAL SUR LEQUEL POINTE L’INVITE DE COMMANDE.
::##############################################################################
::#      -->SYNTAXE : git remote -v
::##############################################################################
git remote -v


::##############################################################################
::# (10.02.)CONNECTER UN DEPÔT DISTANT AU DEPÔT LOCAL COURANT :
::##############################################################################
::#      -->DEPÔT DISTANT A CONNECTER : LE DEPÔT DISTANT FOURNI.
::#      -->DEPÔT LOCAL COURANT :       LE DEPÔT LOCAL SUR LEQUEL POINTE L’INVITE DE COMMANDE.
::##############################################################################
::#      -->SYNTAXE : git remote add [Nom du repository distant] [URL du repository distant]
::##############################################################################
git remote add 02-GestionDeVersions git@gitlab.com:chat_roux/02-GestionDeVersions.git
git remote add 09-BDD-Realisation git@gitlab.com:chat_roux/09-BDD-Realisation.git
git remote add 25-Applications-WebService-MonoModules git@gitlab.com:chat_roux/25-Applications-WebService-MonoModules.git
git remote add 26-Applications-WebService-MultiModules git@gitlab.com:chat_roux/26-Applications-WebService-MultiModules.git


::##############################################################################
::# (10.03.)DECONNECTER UN DEPÔT DISTANT DU DEPÔT LOCAL COURANT :
::##############################################################################
::#      -->DEPÔT DISTANT A DECONNECTER                     : LE DEPÔT DISTANT FOURNI.
::#      -->DEPÔT LOCAL SUR LEQUEL EFFECTUER LA DECONNEXION : LE DEPÔT LOCAL COURANT.
::##############################################################################
::#      -->SYNTAXE : git remote rm [Nom du repository distant]
::##############################################################################
git remote rm 02-GestionDeVersions
git remote rm 09-BDD-Realisation
git remote rm 25-Applications-WebService-MonoModule
git remote rm 26-Applications-WebService-MultiModules
