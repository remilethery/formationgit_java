::##############################################################################
::##############################################################################
::# (12.)CONNEXION ENTRE BRANCHE GIT LOCALE ET BRANCHE GIT DISTANTE :
::##############################################################################
::##############################################################################


::##############################################################################
::# (12.01.)AFFICHER LA LISTE DES BRANCHES DISTANTES CONNECTEES A LA BRANCHE LOCALE COURANTE :
::##############################################################################
::#      -->BRANCHES DISTANTES      : LES BRANCHES DISTANTES CONNECTES A LA BRANCHE LOCAL COURANTE.
::#      -->BRANCHE LOCALE COURANTE : LA BRANCHE LOCALE SUR LAQUELLE POINTE L’INVITE DE COMMANDE.
::##############################################################################
::#      -->SYNTAXE : git checkout master
::#      -->SYNTAXE : git branch -avvv
::##############################################################################
git checkout master
git branch -avvv


::##############################################################################
::# (12.02.)CONNECTER UNE BRANCHE DISTANTE A LA BRANCHE LOCALE COURANTE :
::##############################################################################
::#      -->BRANCHES DISTANTES      : LES BRANCHES DISTANTES CONNECTES A LA BRANCHE LOCAL COURANTE.
::#      -->BRANCHE LOCALE COURANTE : LA BRANCHE LOCALE SUR LAQUELLE POINTE L’INVITE DE COMMANDE.
::##############################################################################
::#      -->SYNTAXE : git checkout master
::#      -->SYNTAXE : git fetch [Nom du dépôt distant] [Nom de la branche distante]
::#      -->SYNTAXE : git branch -u [Nom du dépôt distant]/[Nom de la branche distante]
::##############################################################################
::# -->ATTENTION : LES INSTRUCTIONS CI-DESSOUS DOIVENT ETRE EXECUTEES DANS L'ORDRE !
::##############################################################################
git checkout master
git fetch 26-Applications-WebService-MultiModules master
git branch -u 26-Applications-WebService-MultiModules/master
