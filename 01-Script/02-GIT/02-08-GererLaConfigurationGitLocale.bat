::##############################################################################
::##############################################################################
::# (08.)GERER LA CONFIGURATION "GIT" LOCALE (DE L'ORDINATEUR) :
::##############################################################################
::##############################################################################


::##############################################################################
::# (08.01.)AFFICHER UN ELEMENT DE LA CONFIGURATION "GIT" LOCALE :
::##############################################################################
::# -->ARGUMENT 'user.email' : 
::#         L'ADRESSE MAIL DE L'UTILISATEUR.
::#         (= L'ELEMENT A AFFICHER)
::##############################################################################
::# -->OPTION '--global'     : 
::#         SPECIFIE LE DOMAINE DE VALIDITE DE CETTE CONFIGURATION.
::#         (= CETTE CONFIGURATION SERA VALIDE DANS TOUS LES REPOSITORIES LOCAUX).
::#         (OPTION ABSENTE: CETTE CONFIGURATION SERA VALIDE DANS LE REPOSITORY LOCAL COURANT).
::##############################################################################
git config --global user.email
git config          user.email


::##############################################################################
::# (08.02.)MODIFIER UN ELEMENT DE LA CONFIGURATION "GIT" LOCALE :
::##############################################################################
::# -->ARGUMENT 'user.email' : 
::#         L'ADRESSE MAIL DE L'UTILISATEUR.
::#         (= L'ELEMENT A MODIFIER)
::##############################################################################
::# -->OPTION '--global'     : SPECIFIE LE DOMAINE DE VALIDITE DE CETTE CONFIGURATION.
::#         (= CETTE CONFIGURATION SERA VALIDE DANS TOUS LES REPOSITORIES LOCAUX).
::#         (OPTION ABSENTE: CETTE CONFIGURATION SERA VALIDE DANS LE REPOSITORY LOCAL COURANT).
::##############################################################################
git config --global user.email "tcharou.dalgalian@afpa.fr"
git config          user.email "tcharou.dalgalian@afpa.fr"
