::##############################################################################
::##############################################################################
::# (17.)TRANSFERTS ENTRE BRANCHE LOCALE ET BRANCHE DISTANTE :
::##############################################################################
::##############################################################################


::##############################################################################
::# (17.01.)RECUPERER UNE REPLIQUE LOCALE D'UNE BRANCHE DISTANTE :
::##############################################################################
::# -->BRANCHE DE DESTINATION : UNE REPLIQUE LOCALE DE LA BRANCHE DISTANTE.
::#                             (CETTE REPLIQUE LOCALE SERA AINSI CREEE).
::# -->BRANCHE D'ORIGINE      : LA BRANCHE DISTANTE SPECIFIEE DANS LA COMMANDE.
::#                             ('git fetch ...').
::# -->TYPE DE TRANSFERT      : TRANSFERT AVEC ECRASEMENT.
::##############################################################################
::# --> SYNTAXE : git fetch [Nom du dépôt distant]/[Nom de la branche distante]
::##############################################################################
git fetch 02-GestionDeVersions master
git fetch 09-BDD-Realisation master
git fetch 25-Applications-WebService-MonoModule master
git fetch 26-Applications-WebService-MultiModules master


::##############################################################################
::# (17.02.)TRANSFERER, VERS UNE BRANCHE LOCALE, LE CONTENU D'UNE BRANCHE DISTANTE :
::##############################################################################
::# -->BRANCHE DE DESTINATION : LA BRANCHE LOCALE COURANTE.
::# -->BRANCHE D'ORIGINE      : LA BRANCHE DISTANTE SPECIFIEE DANS LA COMMANDE.
::#                             ('git pull ...')..
::# -->TYPE DE TRANSFERT      : TRANSFERT AVEC FUSION.
::##############################################################################
::# --> SYNTAXE : git pull [Nom du dépôt distant]/[Nom de la branche distante]
::##############################################################################
git pull 02-GestionDeVersions master --allow-unrelated-histories
git pull 09-BDD-Realisation master --allow-unrelated-histories
git pull 25-Applications-WebService-MonoModule master --allow-unrelated-histories
git pull 26-Applications-WebService-MultiModules master --allow-unrelated-histories


::##############################################################################
::# (17.03.)TRANSFERER, VERS UNE BRANCHE DISTANTE, LE CONTENU D'UNE BRANCHE LOCALE :
::##############################################################################
::# -->BRANCHE DE DESTINATION : LA BRANCHE DISTANTE SPECIFIEE DANS LA COMMANDE.
::#                             ('git push ...')
::# -->BRANCHE D'ORIGINE      : LA BRANCHE LOCALE COURANTE.
::# -->TYPE DE TRANSFERT      : TRANSFERT AVEC ECRASEMENT.
::##############################################################################
::# --> SYNTAXE : git push -u [Nom du dépôt distant]/[Nom de la branche distante]
::##############################################################################
git push -u 02-GestionDeVersions master
git push -u 09-BDD-Realisation master
git push -u 25-Applications-WebService-MonoModule master
git push -u 26-Applications-WebService-MultiModules master
