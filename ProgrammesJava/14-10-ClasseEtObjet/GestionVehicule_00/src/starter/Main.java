package starter;
import domain.Vehicule;

public class Main {

	public static void main(String[] args) {
		

		Vehicule Clio = new Vehicule("Clio", "Renault", 6, 1999);
		Clio.Afficher();
		
		Vehicule Ducat = new Vehicule("XR5000", "Ducati", 10, 2006);
		Ducat.Afficher();
		
		Vehicule Bmw = new Vehicule("X3", "BMW", 10, 2019);
		Bmw.Afficher();
					
		
	}

}
