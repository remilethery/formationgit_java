package domain;
import java.util.*;

public class Vehicule {

	private String nomVehicule;
	private String marqueVehicule;
	private int pouissanceVehicule;
	private int annivVehicule;
	
	public Vehicule (String nom, String marque, int pouissance, int anniv)
	{
		this.nomVehicule = nom;
		this.marqueVehicule = marque;
		this.pouissanceVehicule = pouissance;
		this.annivVehicule = anniv;
	}
	
	public void Afficher() {
		System.out.println("***********************************");
		System.out.println("* Nom V�hicule : " + nomVehicule);
		System.out.println("* Marque V�hicule : " + marqueVehicule);
		System.out.println("* Pouissance V�hicule : " + pouissanceVehicule);
		System.out.println("* Date d'anniv V�hicule : " + annivVehicule);
		System.out.println("* Prix Carte grise " + CalculCarteGrise());
		System.out.println("***********************************");
	}
	
	// Ajouter l'ann�e, si ann�es > 10 prixCarteGrise -50% 
	public String CalculCarteGrise() 
	{
		Date date = new Date();// the date instance
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		
		String PrixCarteGrise;
		if ( calendar.get(Calendar.YEAR) > annivVehicule + 10 ) { 
			System.out.println("Ann�e actuelle" + calendar.get(Calendar.YEAR)); 
			PrixCarteGrise = "-50% !";
		}
		else {
			PrixCarteGrise = "Plein Pot lol !";
		}
			
		return PrixCarteGrise;
	}
	
	
}