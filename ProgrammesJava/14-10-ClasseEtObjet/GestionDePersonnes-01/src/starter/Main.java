package starter;

import ui.PersonneUI;

public class Main {

	public static void main(String[] args) {
		
		//////////////////////////////////////////////////////////////
		//(01.)CREER UN OBJET DE TYPE 'PersonneUI'
		//////////////////////////////////////////////////////////////
		PersonneUI personneUI = new PersonneUI();
		
		//////////////////////////////////////////////////////////////
		//(02.)SUR L'OBJET DE TYPE 'PersonneUI', DECLENCHER LA METHODE 'sequencePrincipale'
		//////////////////////////////////////////////////////////////
		personneUI.sequencePrincipale();

	}

}
