
public class Calculatrice {

	
	public static float Addition(float operand1, float operand2) {
		return operand1 + operand2;		
	}
	
	public static float Soustraction(float operand1, float operand2) {
		return operand1 - operand2;
	}
	
	public static float Multiplication(float operand1, float operand2) {
		return operand1 * operand2;
	}
	
	public static float Division(float operand1, float operand2) {
		return operand1 / operand2;
	}
	
}
