package ui;

import java.util.Scanner;

import domain.Personne;

public class PersonneUI {

	
	public void sequencePrincipale() {
		
		Personne personneSaisie = this.saisir();
		this.afficher(personneSaisie);
	}
	
	public Personne saisir() {
		
		System.out.println("+------------------------------------------------------------+");
		System.out.println("| VEUILLEZ SAISIR LES INFORMATIONS RELATIVES A LA PERSONNE : |");
		System.out.println("+------------------------------------------------------------+");

		Scanner scanner = new Scanner(System.in);

		System.out.print("| Nom : ");
		String nom = scanner.nextLine();
		
		System.out.print("| Prenom : ");
		String prenom = scanner.nextLine();

		System.out.print("| Age : ");
		int age = scanner.nextInt();

		Personne personneSaisie = new Personne();
		
		personneSaisie.setNom(nom);
		personneSaisie.setPrenom(prenom);
		personneSaisie.setAge(age);
		
		scanner.close();
		return personneSaisie;
	}
	
	public void afficher(Personne pPersonne) {
		
		System.out.println("+------------------------------------------------------------+");
		System.out.println("| LES INFORMATIONS RELATIVES A LA PERSONNE SAISIE :          |");
		System.out.println("+----------------+-------------------+-----------------------+");
		System.out.println("| NOM :          | PRENOM :          | AGE :                 |");
		System.out.println("+----------------+-------------------+-----------------------+");
		System.out.println("| " + pPersonne.getNom() + "      | " + pPersonne.getPrenom() + "           | " + pPersonne.getAge() + "                    |");
		System.out.println("+----------------+-------------------+-----------------------+");
		
		return;
	}
}
