package domain;

public class Vehicule {

	
	///////////////////////////////////////////////////////////////////////////
	// 1. ATTRIBUTS DE LA CLASSE 
	///////////////////////////////////////////////////////////////////////////
	private String nomVehicule;
	private String marqueVehicule;
	private int pouissance;
	private int anneeImmat;
	
	///////////////////////////////////////////////////////////////////////////
	// 2. CONSTRUCTEURS DE LA CLASSE 
	///////////////////////////////////////////////////////////////////////////
	public Vehicule() {}

	public Vehicule(String pNom, String pPrenom, int pPouissance, int pAnneeImmat) {
		
		this.nomVehicule = pNom;
		this.marqueVehicule = pPrenom;
		this.pouissance = pPouissance;
		this.anneeImmat = pAnneeImmat;
	}
	
	///////////////////////////////////////////////////////////////////////////
	// 3. METHODES DE LA CLASSE 
	///////////////////////////////////////////////////////////////////////////
	public void afficher() {
		System.out.println("**********AFFICHAGE D'UNE PERSONNE********************");
		System.out.println("Nom V�hicule : " + nomVehicule);
		System.out.println("Marque V�hicule : " + marqueVehicule);
		System.out.println("Pouissance : " + pouissance);
		System.out.println("Ann�e Immat : " + anneeImmat);
		System.out.println("*****************************************************");
	}
		
	///////////////////////////////////////////////////////////////////////////
	// 4. ACCESSEURS EN LECTURE DE LA CLASSE 
	///////////////////////////////////////////////////////////////////////////
	public String getNomVehicule()           { return nomVehicule; }
	public String getMarqueVehicule()        { return marqueVehicule; }
	public int getPouissance()           { return pouissance; }
	public int getAnneeImmat()			{return anneeImmat;}
	
	///////////////////////////////////////////////////////////////////////////
	// 5. ACCESSEURS EN ECRITURE DE LA CLASSE 
	///////////////////////////////////////////////////////////////////////////
	public void setNomVehicule(String nomVehicule)       { this.nomVehicule    = nomVehicule; }
	public void setMarqueVehicule(String marqueVehicule) { this.marqueVehicule = marqueVehicule; }
	public void setAge(int pouissance)          { this.pouissance = pouissance; }
	public void setAnneeImmat(int annee)          { this.anneeImmat = anneeImmat; }
	
	
}
