package ui;

import java.util.Scanner;

import domain.Vehicule;



public class VehiculeUI {


	
	public void sequencePrincipale() {
		
		Vehicule vehiculeSaisi = this.saisir();
		this.afficher(vehiculeSaisi);
			
		
	}
	
	public Vehicule saisir() {
		
		System.out.println("+------------------------------------------------------------+");
		System.out.println("| VEUILLEZ SAISIR LES INFORMATIONS RELATIVES AU VEHICULE : |");
		System.out.println("+------------------------------------------------------------+");

		Scanner scanner = new Scanner(System.in);

		System.out.print("| Nom V�hicule : ");
		String nom = scanner.nextLine();
		
		System.out.print("| Marque V�hicule : ");
		String prenom = scanner.nextLine();

		System.out.print("| Pouissance : ");
		int pouissance = scanner.nextInt();
		
		System.out.print("| Ann�e d'immatriculation : ");
		int anneeImmat = scanner.nextInt();

		Vehicule vehiculeSaisi = new Vehicule(nom, prenom, pouissance, anneeImmat);
		
		// scanner.close();
		return vehiculeSaisi;
	}
	
	public void afficher(Vehicule vehicule) {
		
		System.out.println("+------------------------------------------------------------+");
		System.out.println("| LES INFORMATIONS RELATIVES AU VEHICULE   SAISIE :          |");
		System.out.println("+----------------+-------------------+-----------------------+");
		System.out.println("| NOM :          | PRENOM :          | AGE :                 |");
		System.out.println("+----------------+-------------------+-----------------------+");
		System.out.println("| " + vehicule.getNomVehicule() + "           | " + vehicule.getMarqueVehicule() + "           | " + vehicule.getPouissance() + "                    |");
		System.out.println("+----------------+-------------------+-----------------------+");
		
		return;
	}
	
	
}
