package starter;

import business.entity.Personne;

public class Main {

	public static void main(String[] args) {
		

		////////////////////////////////////////////////////////////////////
		//(01.)CREER UN OBJET DE TYPE 'Personne'
		////////////////////////////////////////////////////////////////////
		Personne personneObject = new Personne(); 
		
		////////////////////////////////////////////////////////////////////
		//(02.)RECUPERER L'OBJET SUIVANT :
		//     -->L'OBJET DE TYPE 'META-CLASSE' CORRESPONDANT A L'OBJET CREE 
		////////////////////////////////////////////////////////////////////
		Class<?> personneClass = personneObject.getClass();
		
		////////////////////////////////////////////////////////////////////
		//(03.)RECUPERER LE NOM DE L'OBJET SUIVANT :
		//     -->L'OBJET DE TYPE 'META-CLASSE' RECUPERE PRECEDEMMENT
		////////////////////////////////////////////////////////////////////
		String className = personneClass.getName();
		
		////////////////////////////////////////////////////////////////////
		//(04.)AFFICHER LE TYPE DE L'OBJET CREE PRECEDEMMENT
		////////////////////////////////////////////////////////////////////
		System.out.println("+--------------------------------------------+");
		System.out.println("| TYPE DE L'OBJET :                          |");
		System.out.println("+--------------------------------------------+");
		System.out.println("| 'Personne' ?                               |");
		System.out.println("+--------------------------------------------+");
		System.out.println("| " + (personneObject instanceof Personne));
		System.out.println("+--------------------------------------------+");

		////////////////////////////////////////////////////////////////////
		//(05.)AFFICHER LE NOM DE L'OBJET DE TYPE 'META-CLASSE'
		////////////////////////////////////////////////////////////////////
		System.out.println("+--------------------------------------------+");
		System.out.println("| META-CLASSE DE L'OBJET :                   |");
		System.out.println("+--------------------------------------------+");
		System.out.println("| 'Personne' ?                               |");
		System.out.println("+--------------------------------------------+");
		System.out.println("| " + className);
		System.out.println("+--------------------------------------------+");
	}
}
