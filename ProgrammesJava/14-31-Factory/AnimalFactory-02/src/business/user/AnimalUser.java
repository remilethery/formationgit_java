package business.user;

import business.entity.Animal;
import business.entity.Chat;
import business.entity.Chien;
import business.factory.AnimalFactory;

/**
 * <b>CETTE CLASSE EST UN UTILISATEUR D'OBJETS DU TYPE 'Animal' :</b>
 * @author Tcharou
 *
 */
public class AnimalUser {

	
	private AnimalFactory animalFactory;
	
	
	/**
	 * <b>CONSTRUCTEUR AVEC 1 ARGUMENT</b>
	 * @param pAnimalFactory
	 */
	public AnimalUser(AnimalFactory pAnimalFactory) {
		
		this.animalFactory = pAnimalFactory;
	}
	
	
	/**
	 * <b>EFFECTUER UN TRAITEMENT</b>
	 * @return
	 */
	public void work() {
		
		//////////////////////////////////////////////////////////////////////////////////////
		//(01.)DEMANDER A LA FABRIQUE D'ANIMAUX D'EFFECTUER LA CREATION D'OBJET SUIVANTE :
		//     -->OBJET A CREER      : UN OBJET DE TYPE 'Animal' 
		//////////////////////////////////////////////////////////////////////////////////////
		Animal animalCree = this.animalFactory.create();
		
		//////////////////////////////////////////////////////////////////////////////////////
		//(02.)AFFICHER LE TYPE DE L'OBJET CREE
		//////////////////////////////////////////////////////////////////////////////////////
		System.out.println("+--------------------------------------------+");
		System.out.println("| TYPE DE L'OBJET 'Animal' CREE :            |");
		System.out.println("+----------------+---------------------------+");
		System.out.println("| TYPE 'Chien' : | " + (animalCree instanceof Chien));
		System.out.println("+----------------+---------------------------+");
		System.out.println("| TYPE 'Chat' :  | " + (animalCree instanceof Chat));
		System.out.println("+----------------+---------------------------+");
		
		//////////////////////////////////////////////////////////////////////////////////////
		//(03.)DECLENCHER LE COMPORTEMENT 'crier' DE L'OBJET CREE
		//////////////////////////////////////////////////////////////////////////////////////
		animalCree.crier();
	}

}
