package business.factory;
import business.entity.Animal;
import business.entity.Chien;

/**
 * <b>CETTE CLASSE EST UNE FABRIQUE D'OBJETS DU TYPE 'Animal' :</b>
 * @author Tcharou
 *
 */
public class AnimalFactory {
	
	
	private String animalType;

	
	/**
	 * <b>CONSTRUCTEUR SANS ARGUMENT</b>
	 */
	public AnimalFactory() {}
	
	
	/**
	 * <b>CONFIGURER LA FABRICATION D'ANIMAUX</b>
	 * @param pAnimalType Le type d'animaux � fabriquer.
	 */
	public void configure(String pAnimalType) {
		this.animalType = pAnimalType;
	}

	
	/**
	 * <b>CREER L'OBJET DEMANDE</b>
	 * @return L'animal fabriqu�.
	 */
	public Animal create() {
		
		/////////////////////////////////////////////////////////////
		//(01.)CREER LES META-CLASSES SUIVANTES :
		//     -->UNE META-CLASSE DE LA CLASSE "business.entity.Animal"
		/////////////////////////////////////////////////////////////
		Class<?> metaClasseAnimal = null;
		try {
			metaClasseAnimal = Class.forName(this.animalType);
			
		} catch (ClassNotFoundException e) {
			System.out.println("Cr�ation d'une m�ta-classe � partir de son nom -- La classe est introuvable");
		}
		/////////////////////////////////////////////////////////////
		//(02.)CREER LES OBJETS SUIVANTS :
		//     -->UN OBJET DE TYPE "business.entity.Chien"
		/////////////////////////////////////////////////////////////
		Animal animalCree = null;
		try {
			animalCree = (Animal) metaClasseAnimal.newInstance();
			
		} catch (InstantiationException e) {
			System.out.println("Cr�ation d'un objet � partir de sa m�ta-classe -- La classe n'est pas instanciable");
			
		} catch (IllegalAccessException e) {
			System.out.println("Cr�ation d'un objet � partir de sa m�ta-classe -- La classe n'est pas accessible");
		}
		/////////////////////////////////////////////////////////////
		//(03.)RENVOYER L'OBJET CREE :
		/////////////////////////////////////////////////////////////
		return animalCree;
	}
}
