package business.entity;

public abstract class AbstractPersonne implements ITravailleur {
	
	
	private long id;
	private String nom;
	private String prenom;
	private int age;
	
	
	public AbstractPersonne() {}

	
	public long   getId    () { return id; }
	public String getNom   () { return nom; }
	public String getPrenom() { return prenom; }
	public int    getAge   () { return age; }

	
	public void setId    (long   id    ) { this.id     = id;     }
	public void setNom   (String nom   ) { this.nom    = nom;    }
	public void setPrenom(String prenom) { this.prenom = prenom; }
	public void setAge   (int    age   ) { this.age    = age;    }
}
