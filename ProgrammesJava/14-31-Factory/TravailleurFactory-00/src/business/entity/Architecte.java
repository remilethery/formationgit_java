package business.entity;

public class Architecte extends AbstractPersonne {

	@Override
	public void travailler() {
		System.out.println("J'effectue la mise en place de l'architecture...");
	}

}
