package business.factory;
import business.entity.Animal;
import business.entity.Chien;

/**
 * <b>CETTE CLASSE EST UNE FABRIQUE D'OBJETS DU TYPE 'Animal' :</b>
 * @author Tcharou
 *
 */
public class AnimalFactory {
	
	
	/**
	 * <b>CONSTRUCTEUR SANS ARGUMENT</b>
	 */
	public AnimalFactory() {}
	
	
	/**
	 * <b>CREER L'OBJET DEMANDE</b>
	 * @return
	 */
	public Animal create() {
		
		/////////////////////////////////////////////////////////////
		//(01.)CREER LES META-CLASSES SUIVANTES :
		//     -->UNE META-CLASSE DE LA CLASSE "business.entity.Chien"
		/////////////////////////////////////////////////////////////
		Class<?> metaClasseChien = null;
		try {
			metaClasseChien = Class.forName("business.entity.Chien");
			
		} catch (ClassNotFoundException e) {
			System.out.println("Cr�ation d'une m�ta-classe � partir de son nom -- La classe est introuvable");
		}
		/////////////////////////////////////////////////////////////
		//(02.)CREER LES OBJETS SUIVANTS :
		//     -->UN OBJET DE TYPE "business.entity.Chien"
		/////////////////////////////////////////////////////////////
		Chien chienCree = null;
		try {
			chienCree = (Chien) metaClasseChien.newInstance();
			
		} catch (InstantiationException e) {
			System.out.println("Cr�ation d'un objet � partir de sa m�ta-classe -- La classe n'est pas instanciable");
			
		} catch (IllegalAccessException e) {
			System.out.println("Cr�ation d'un objet � partir de sa m�ta-classe -- La classe n'est pas accessible");
		}
		/////////////////////////////////////////////////////////////
		//(03.)TRANSTYPER L'OBJET DE LA MANIERE SUIVANTE (TRANSTYPAGE IMPLICITE) :
		//     -->TYPE DE DESTINATION : "business.entity.Animal"
		/////////////////////////////////////////////////////////////
		Animal animalCree = chienCree;
		
		/////////////////////////////////////////////////////////////
		//(03.)RENVOYER L'OBJET CREE :
		/////////////////////////////////////////////////////////////
		return animalCree;
	}
		
		
}
