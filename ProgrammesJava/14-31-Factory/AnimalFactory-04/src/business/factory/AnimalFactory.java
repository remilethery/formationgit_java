package business.factory;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.reflections.Reflections;

import business.entity.Animal;
import business.entity.ICrieur;


/**
 * <b>CETTE CLASSE EST UNE FABRIQUE D'OBJETS DU TYPE 'Animal' :</b>
 * @author Tcharou
 *
 */
public class AnimalFactory {
	
	
	/**
	 * <b>MAP DESTINEE A STOCKER DES COUPLES DECRITS CI-DESSOUS :</b><br>
	 * <br>
	 * -->CLES    : LES TYPES D'OBJETS A FABRIQUER<br>
	 * -->VALEURS : LES FQN (FULLY QUALIFIED NAME) DES TYPE D'OBJETS A FABRIQUER
	 */
	private Map<String, String> animalTypes;
	
	/**
	 * <b>CHAINE DE CARACTERES DESTINEE A STOCKER LE TYPE D'OBJET A FABRIQUER</b>
	 */
	private String animalType;
	
	
	/**
	 * <b>CONSTRUCTEUR SANS ARGUMENT</b>
	 */
	public AnimalFactory() {
		
		///////////////////////////////////////////////////////////////////
		//(01.)CREER UN OBJET CAPABLE D'EXPLORATION UNE ARBORESCENCE DE CLASSES 
		///////////////////////////////////////////////////////////////////
		Reflections reflections = new Reflections("business.entity");

		///////////////////////////////////////////////////////////////////
		//(02.)????????????????????????
		//     (CREER UNE COLLECTION D'OBJETS HERITANT DU TYPE 'Animal')  
		///////////////////////////////////////////////////////////////////
	    Set<Class<? extends ICrieur>> animalSubTypes = reflections.getSubTypesOf(ICrieur.class);
	     
		///////////////////////////////////////////////////////////////////
		//(03.)CREER LA MAP 
		///////////////////////////////////////////////////////////////////
		this.animalTypes = new HashMap<String, String>();

		///////////////////////////////////////////////////////////////////
		//(04.)?????????????????????????
		//     (ALIMENTER LA MAP A L'AIDE DE LA COLLECTION D'OBJETS RECUPEREE)
		///////////////////////////////////////////////////////////////////
	    for (Class<? extends ICrieur> subType : animalSubTypes) {
	    	
	    	String subTypeSimpleName = subType.getSimpleName();
	    	String subTypeFullyQualifedName = subType.getName();
	    	
			this.animalTypes.put(subTypeSimpleName, subTypeFullyQualifedName);
			
			System.out.println("Animal-Types Simple Names          : " + subTypeSimpleName       );
			System.out.println("Animal-Types Fully Qualified Names : " + subTypeFullyQualifedName);
		}
	}
	
	/**
	 * <b>CONFIGURER LA FABRICATION D'ANIMAUX</b>
	 * @param pAnimalType Le type d'animaal � fabriquer.
	 */
	public void configure(String pAnimalType) {
		
		/////////////////////////////////////////////////////////////
		//(00.)EXTRAIRE DE LA MAP L'OBJET SUIVANT :
		//     ->LE FQN (FULLY QUALIFIED NAME) CORRESPONDANT AU TYPE D'OBJET A CREER.
		/////////////////////////////////////////////////////////////
		String animaTypeFQN = this.animalTypes.get(pAnimalType);
		
		/////////////////////////////////////////////////////////////
		//(01.)ALIMENTER L'ATTRIBUT 'animalType'
		/////////////////////////////////////////////////////////////
		this.animalType = animaTypeFQN;
	}
	
	/**
	 * <b>CREER L'OBJET DEMANDE</b>
	 * @return L'animal fabriqu�.
	 */
	public Animal create() {
		
		/////////////////////////////////////////////////////////////
		//(01.)CREER LES META-CLASSES SUIVANTES :
		//     -->UNE META-CLASSE DE LA CLASSE "business.entity.Animal"
		/////////////////////////////////////////////////////////////
		Class<?> metaClasseAnimal = null;
		try {
			metaClasseAnimal = Class.forName(this.animalType);
			
		} catch (ClassNotFoundException e) {
			System.out.println("Cr�ation d'une m�ta-classe � partir de son nom -- La classe est introuvable");
		}
		/////////////////////////////////////////////////////////////
		//(02.)CREER UN OBJET DU TYPE DECRIT CI-DESSOUS :
		//     -->LE TYPE POUR LEQUEL LA FABRIQUE A ETE CONFIGUREE.
		/////////////////////////////////////////////////////////////
		Animal animalCree = null;
		try {
			animalCree = (Animal) metaClasseAnimal.newInstance();
			
		} catch (InstantiationException e) {
			System.out.println("Cr�ation d'un objet � partir de sa m�ta-classe -- La classe n'est pas instanciable");
			
		} catch (IllegalAccessException e) {
			System.out.println("Cr�ation d'un objet � partir de sa m�ta-classe -- La classe n'est pas accessible");
		}
		/////////////////////////////////////////////////////////////
		//(03.)RENVOYER L'OBJET CREE :
		/////////////////////////////////////////////////////////////
		return animalCree;
	}
}
