package model;

public class Personne {

	
	///////////////////////////////////////////////////////////////////////////
	// 1. ATTRIBUTS DE LA CLASSE 
	///////////////////////////////////////////////////////////////////////////
	static int compteur;
	static Personne instance;
	
	private String nom;
	private String prenom;
	private int age;
	
	///////////////////////////////////////////////////////////////////////////
	// 2. CONSTRUCTEURS DE LA CLASSE 
	///////////////////////////////////////////////////////////////////////////
	private Personne() {
		
		super();
		Personne.compteur++;
	}

	/**
	 * @param nom Nom de la personne cr��e
	 * @param prenom Nom de la personne cr��e
	 * @param pAge Age de la personne cr��e
	 */
	private Personne(String pNom, String pPrenom, int pAge) {
		
		super();
		Personne.compteur++;
		this.nom = pNom;
		this.prenom = pPrenom;
		this.age = pAge;
	}
	
	/**
	 * @param nom Nom de la personne cr��e
	 * @param prenom Nom de la personne cr��e
	 */
	private Personne(String nom, String prenom) {
		
		super();
		Personne.compteur++;
		this.nom = nom;
		this.prenom = prenom;
	}


	///////////////////////////////////////////////////////////////////////////
	// 3. METHODES DE LA CLASSE 
	///////////////////////////////////////////////////////////////////////////
	public static synchronized Personne getInstance() {
		
		if(Personne.instance == null) {
			
			synchronized(Personne.class){
				
				if (Personne.instance == null) {
					
					Personne.instance = new Personne();
					System.out.println("Nous venons d'instancier une nouvelle personne !!!");
				}
			}
			
		} else {
			System.out.println("Nous n'avons rien instanci� du tout. L'objet existe d�j� !!!");;
		}
		return Personne.instance;
	}
	
	
	
	public void afficher() {
		System.out.println("**********AFFICHAGE D'UNE PERSONNE********************");
		System.out.println("Compteur : " + compteur);
		System.out.println("Nom : " + nom);
		System.out.println("Pr�nom : " + prenom);
		System.out.println("Age : " + age);
		System.out.println("*****************************************************");
	}
	
	///////////////////////////////////////////////////////////////////////////
	// 4. ACCESSEURS EN LECTURE DE LA CLASSE 
	///////////////////////////////////////////////////////////////////////////
	/**
	 * @return Nom de la personne
	 */
	public String getNom()           { return nom; }
	/**
	 * @return Prenom de la personne
	 */
	public String getPrenom()        { return prenom; }
	/**
	 * @return Age de la personne
	 */
	public int    getAge()           { return age; }
	
	///////////////////////////////////////////////////////////////////////////
	// 5. ACCESSEURS EN ECRITURE DE LA CLASSE 
	///////////////////////////////////////////////////////////////////////////
	public void setNom(String nom)       { this.nom    = nom; }
	public void setPrenom(String prenom) { this.prenom = prenom; }
	public void setAge(int age)          { this.age    = age; }
}
