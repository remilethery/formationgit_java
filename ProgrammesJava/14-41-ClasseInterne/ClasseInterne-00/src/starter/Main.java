package starter;
import model.Personne;

public class Main {

	public static void main(String[] args) {

		
		///////////////////////////////////////////////////////////////////////
		// RECUPERER L'INSTANCE EXISTANTE DE L'OBJET DE TYPE 'PERSONNE'
		///////////////////////////////////////////////////////////////////////
		
		Personne personne1 = Personne.getInstance();
		System.out.println(personne1.toString());
		
		Personne personne2 = Personne.getInstance();
		System.out.println(personne2.toString());
		
		Personne personne3 = Personne.getInstance();
		System.out.println(personne3.toString());
		
		Personne personne4 = Personne.getInstance();
		System.out.println(personne4.toString());
	}
}
