package domain;

public class Personne {

	
	///////////////////////////////////////////////////////////////////////////
	// 1. ATTRIBUTS DE LA CLASSE 
	///////////////////////////////////////////////////////////////////////////
	private static int compteur;
	private static Personne instance;
	
	private String nom;
	private String prenom;
	private int age;
	
	///////////////////////////////////////////////////////////////////////////
	// 2. CONSTRUCTEURS DE LA CLASSE 
	///////////////////////////////////////////////////////////////////////////
	private Personne() {
		
		Personne.compteur++;
	}

	///////////////////////////////////////////////////////////////////////////
	// 3. METHODES DE CLASSE 
	///////////////////////////////////////////////////////////////////////////
	public static Personne getInstance() {
		
		if(Personne.instance == null) {
			
			Personne.instance = new Personne();
			System.out.println("Nous venons d'instancier une nouvelle personne !!!");
			
		} else {
			System.out.println("Nous n'avons rien instanci� du tout. L'objet existe d�j� !!!");;
		}
		return Personne.instance;
	}

	
	///////////////////////////////////////////////////////////////////////////
	// 4. METHODES D'INSTANCE
	///////////////////////////////////////////////////////////////////////////
	public void afficher() {
		System.out.println("**********AFFICHAGE D'UNE PERSONNE********************");
		System.out.println("Nombre d'objets de type 'Personne' pr�sent dans le tas : " + Personne.compteur);
		System.out.println("Nom      : " + nom     );
		System.out.println("Pr�nom   : " + prenom  );
		System.out.println("Age      : " + age     );
		System.out.println("*****************************************************");
	}
	
	public String getNom ()           { return nom;    }
	public String getPrenom ()        { return prenom; }
	public int    getAge ()           { return age;    }
	
	public void setNom   (String nom   ) { this.nom    = nom;    }
	public void setPrenom(String prenom) { this.prenom = prenom; }
	public void setAge   (int    age   ) { this.age    = age;    }
}
