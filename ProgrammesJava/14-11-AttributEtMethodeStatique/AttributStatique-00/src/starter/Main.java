package starter;
import domain.Personne;

public class Main {

	public static void main(String[] args) {

		
		///////////////////////////////////////////////////////////////////////
		// CREER DES OBJETS DU TYPE 'PERSONNE'
		///////////////////////////////////////////////////////////////////////
		//TODO CREATION DE PERSONNES !!!
		
		System.out.println("Compteur :" + Personne.compteur);
		
		Personne personne = new Personne("Dalgalian", "Tcharou" , 49);
		personne.afficher();
		
		Personne personne2 = new Personne("Pagan", "Jean-Jacques" , 44);
		personne2.afficher();
		
		Personne personne3 = new Personne("Bachri", "Amin" , 42);
		personne3.afficher();
		
	}

}
