package domain;
/**
 * 
 */

/**
 * @author 1603599
 *
 */
public class Personne implements Comparable<Personne> {

	private String nom;
	private String prenom;
	private int age;
	
	public Personne() {
		
		this.nom    = "";
		this.prenom = "";
		this.age    = 0;
	}
	
	public Personne(String pNom, String pPrenom, int pAge) {
		
		this.nom    = pNom;
		this.prenom = pPrenom;
		this.age    = pAge;
	}
	
	/**
	 * <b>COMPARER LA PERSONNE FOURNIE EN PARAMETRE AVEC L'OBJET COURANT (DE TYPE PERSONNE)</b>
	 * 
	 * @param pPersonne L'objet 'Personne' pass� en param�tre
	 * 
	 * @return   int<br/>
	 *            0  si (l'age de l'objet courant) = (l'age de l'objet pass� en param�tre).<br/>
	 *           -1  si (l'age de l'objet courant) < (l'age de l'objet pass� en param�tre).<br/>
	 *           +1  si (l'age de l'objet courant) > (l'age de l'objet pass� en param�tre).
	 */
	@Override
	public int compareTo(Personne pPersonne) {
		
		////////////////////////////////////////////////////////////////////////
		//COMPARER LA PERSONNE FOURNIE EN PARAMETRE AVEC L'OBJET COURANT (DE TYPE PERSONNE)
		////////////////////////////////////////////////////////////////////////
		 if (this.age < pPersonne.age) {
			System.out.println("(l'age de l'objet courant) < (l'age de l'objet pass� en param�tre)");
			return -1;
			
		} else if (this.age > pPersonne.age) {
			System.out.println("(l'age de l'objet courant) > (l'age de l'objet pass� en param�tre)");
			return 1;
			
		} else  {
			System.out.println("(l'age de l'objet courant) = (l'age de l'objet pass� en param�tre)");
			return 0;
		}
	}

	public String getNom   () { return nom;    }
	public String getPrenom() { return prenom; }
	public int    getAge   () { return age;    }

	public void setNom   (String pNom   ) { this.nom    = pNom;    }
	public void setPrenom(String pPrenom) { this.prenom = pPrenom; }
	public void setAge   (int    pAge   ) { this.age    = pAge;    }
}
