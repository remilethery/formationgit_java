/**
 * 
 */
package user;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import domain.Personne;

/**
 * @author 1603599
 *
 */
public class PersonneUser {

	
	/**
	 * <b>CREER DES PERSONNES ET LES COMPARER</b>
	 */
	public void execute(){
		
		///////////////////////////////////////////////////////////////////////
		//(1.)CREER DES PERSONNES
		///////////////////////////////////////////////////////////////////////
		Personne personne00 = new Personne("Dalgalian   ", "Tcharou1   ", 48);
		Personne personne01 = new Personne("Dalgalian   ", "Tcharou2   ", 48);
		Personne personne02 = new Personne("Gineste     ", "Michel     ", 63);
		Personne personne03 = new Personne("Girard      ", "Dylan      ", 22);
		Personne personne04 = new Personne("Hor         ", "William    ", 26);
		Personne personne05 = new Personne("Timsit      ", "Hicham     ", 34);
		Personne personne06 = new Personne("Esposito    ", "Francesco  ", 34);
		Personne personne07 = new Personne("Lefaix      ", "Jimmy      ", 25);
		Personne personne08 = new Personne("Ralaidovy   ", "Tony       ", 32);
		Personne personne09 = new Personne("Brasseur    ", "Gilles     ", 29);
		Personne personne10 = new Personne("Leconte     ", "Jonathan   ", 28);
		Personne personne11 = new Personne("Millot      ", "Jerome     ", 34);
		Personne personne12 = new Personne("Hyolle      ", "Hugo       ", 30);
		Personne personne13 = new Personne("Miot        ", "Roger      ", 38);
		Personne personne14 = new Personne("Kabeya      ", "Patrick    ", 33);
		Personne personne15 = new Personne("Nsoki Miansi", "Jean Claude", 35);
		Personne personne16 = new Personne("Holding     ", "John       ", 31);
		Personne personne17 = new Personne("Mekenfes    ", "Intissar   ", 28);
		
		///////////////////////////////////////////////////////////////////////
		//(2.)CREER UNE LISTE
		///////////////////////////////////////////////////////////////////////
		List<Personne> personnes = new ArrayList<Personne>();
		
		///////////////////////////////////////////////////////////////////////
		//(3.)SAUVEGARDER TOUTES LES PERSONNES DANS LA LISTE
		///////////////////////////////////////////////////////////////////////
	    personnes.add(personne00);
	    personnes.add(personne01);
	    personnes.add(personne02);
	    personnes.add(personne03);
	    personnes.add(personne04);
	    personnes.add(personne05);
	    personnes.add(personne06);
	    personnes.add(personne07);
	    personnes.add(personne08);
	    personnes.add(personne09);
	    personnes.add(personne10);
	    personnes.add(personne11);
	    personnes.add(personne12);
	    personnes.add(personne13);
	    personnes.add(personne14);
	    personnes.add(personne15);
	    personnes.add(personne16);
	    personnes.add(personne17);
 
		///////////////////////////////////////////////////////////////////////
		//(4.)AFFICHER LE CONTENU DE LA LISTE CREEE PRECEDEMMENT
		///////////////////////////////////////////////////////////////////////
	    this.afficherListe(personnes);
	    
		///////////////////////////////////////////////////////////////////////
		//(5.)ORDONNER (PAR AGE CROISSANT) LE CONTENU DE LA LISTE CREEE PRECEDEMMENT
		///////////////////////////////////////////////////////////////////////
	    Collections.sort(personnes);

	    ///////////////////////////////////////////////////////////////////////
		//(6.)AFFICHER LE CONTENU DE LA LISTE CREEE PRECEDEMMENT
		///////////////////////////////////////////////////////////////////////
	    this.afficherListe(personnes);
	}

	/**
	 * AFFICHER LE CONTENU DE LA LISTE FOURNIE
	 * @param pPersonnes
	 */
	private void afficherListe(List<Personne> pPersonnes) {
		
		///////////////////////////////////////////////////////////////////////
		//(1.)AFFICHER TOUTES LES PERSONNES DE LA LISTE
		///////////////////////////////////////////////////////////////////////
	  	System.out.println(" TAILLE DE LA LISTE :" + pPersonnes.size());
	  	
    	System.out.println("+---------------------------------------------------+");
    	System.out.println("|          CONTENU DE LA LISTE                      |");
    	System.out.println("+-------------+-----------+------------+------------+");
    	System.out.println("| INDEX :     | NOM :     | PRENOM :   | AGE :      |");
    	System.out.println("+-------------+-----------+------------+------------+");

    	for (int cpt=0; cpt < pPersonnes.size() ; cpt++ ) {
	    	
    	   	Personne currentPersonne  = pPersonnes.get(cpt);
        	System.out.println("| " + cpt +    "        | " + currentPersonne.getNom()  
        											+  "| " + currentPersonne.getPrenom() 
        											+  "| " + currentPersonne.getAge() +     "|");
	    }
    	System.out.println("+-------------+-----------+------------+------------+");
	};
}
