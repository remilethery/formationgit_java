/**
 * 
 */
package user;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import domain.Personne;

/**
 * @author Tcharou
 *
 */
public class PersonneUser {
	
	/**
	 * <b>CREER DES PERSONNES ET LES AFFICHER</b>
	 */
	public void execute(){
		
		///////////////////////////////////////////////////////////////////////
		//(01.)CREER DES PERSONNES
		///////////////////////////////////////////////////////////////////////
		Personne personne00 = new Personne("Dalgalian   ", "Tcharou1   ", 48);
		Personne personne01 = new Personne("Dalgalian   ", "Tcharou2   ", 48);
		Personne personne02 = new Personne("Gineste     ", "Michel     ", 63);
		Personne personne03 = new Personne("Girard      ", "Dylan      ", 22);
		Personne personne04 = new Personne("Hor         ", "William    ", 26);
		Personne personne05 = new Personne("Timsit      ", "Hicham     ", 34);
		Personne personne06 = new Personne("Esposito    ", "Francesco  ", 34);
		Personne personne07 = new Personne("Lefaix      ", "Jimmy      ", 25);
		Personne personne08 = new Personne("Ralaidovy   ", "Tony       ", 32);
		Personne personne09 = new Personne("Brasseur    ", "Gilles     ", 29);
		Personne personne10 = new Personne("Leconte     ", "Jonathan   ", 28);
		Personne personne11 = new Personne("Millot      ", "Jerome     ", 34);
		Personne personne12 = new Personne("Hyolle      ", "Hugo       ", 30);
		Personne personne13 = new Personne("Miot        ", "Roger      ", 38);
		Personne personne14 = new Personne("Kabeya      ", "Patrick    ", 33);
		Personne personne15 = new Personne("Nsoki Miansi", "Jean Claude", 35);
		Personne personne16 = new Personne("Holding     ", "John       ", 31);
		Personne personne17 = new Personne("Mekenfes    ", "Intissar   ", 28);
		
		///////////////////////////////////////////////////////////////////////
		//(02.)CREER UNE MAP
		///////////////////////////////////////////////////////////////////////
		Map<Integer, Personne> personnes = new HashMap<Integer, Personne>();
		
		///////////////////////////////////////////////////////////////////////
		//(03.)SAUVEGARDER TOUTES LES PERSONNES DANS LA MAP
		///////////////////////////////////////////////////////////////////////
	    personnes.put (Integer.valueOf ( 0), personne00);
	    personnes.put (Integer.valueOf ( 1), personne01);
	    personnes.put (Integer.valueOf ( 2), personne02);
	    personnes.put (Integer.valueOf ( 3), personne03);
	    personnes.put (Integer.valueOf ( 4), personne04);
	    personnes.put (Integer.valueOf ( 5), personne05);
	    personnes.put (Integer.valueOf ( 6), personne06);
	    personnes.put (Integer.valueOf ( 7), personne07);
	    personnes.put (Integer.valueOf ( 8), personne08);
	    personnes.put (Integer.valueOf ( 9), personne09);
	    personnes.put (Integer.valueOf (10), personne10);
	    personnes.put (Integer.valueOf (11), personne11);
	    personnes.put (Integer.valueOf (12), personne12);
	    personnes.put (Integer.valueOf (13), personne13);
	    personnes.put (Integer.valueOf (14), personne14);
	    personnes.put (Integer.valueOf (15), personne15);
	    personnes.put (Integer.valueOf (16), personne16);
	    personnes.put (Integer.valueOf (17), personne17);
 
		///////////////////////////////////////////////////////////////////////
		//(04.)AFFICHER LE CONTENU DE LA MAP CREEE PRECEDEMMENT
		///////////////////////////////////////////////////////////////////////
	    this.afficherMap(personnes);
	}

	/**
	 * AFFICHER LE CONTENU DE LA MAP FOURNIE
	 * @param pPersonnes
	 */
	private void afficherMap (Map<Integer, Personne> pPersonnes) {
		
		///////////////////////////////////////////////////////////////////////
		//(01.)EXTRAIRE LE CONTENU DE LA MAP (SOUS LA FORME D'UNE LISTE DE COUPLES)
		///////////////////////////////////////////////////////////////////////
		Set<Entry<Integer, Personne>> couples = pPersonnes.entrySet();
		
		///////////////////////////////////////////////////////////////////////
		//(02.)PARCOURIR LA LISTE DES COUPLES EXTRAITE PRECEDEMMENT
		///////////////////////////////////////////////////////////////////////
	  	System.out.println(" TAILLE DE LA MAP         : " + pPersonnes.size());
	  	System.out.println(" TAILLE DU SET DE COUPLES : " + couples.size());
	  	
    	System.out.println("+---------------------------------------------------+");
    	System.out.println("|          CONTENU DE LA MAP                        |");
    	System.out.println("+-------------+-------------------------------------+");
    	System.out.println("| INDEX :     | PERSONNE :                          |");
    	System.out.println("+-------------+-------------------------------------+");

    	for (Entry<Integer, Personne> couple : couples) {
    		
    		Integer cle = couple.getKey();
    		Personne personne = couple.getValue();
    		
        	System.out.println("| " + cle +    "        | " + personne.toString() + " |");
	    }
    	System.out.println("+-------------+-------------------------------------+");
	}
}
