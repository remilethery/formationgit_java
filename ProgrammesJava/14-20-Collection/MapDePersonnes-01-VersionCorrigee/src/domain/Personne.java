package domain;
/**
 * 
 */

/**
 * <b>CLASSE QUI IMPLEMENTE LES PROPRIETES ET COMPORTEMENTS DE L'ENTITE 'PERSONNE'</b>
 * 
 * @author Tcharou DALGALIAN
 *
 */
public class Personne implements Comparable<Personne> {

	
	private String nom;
	private String prenom;
	private int age;
	
	
	public Personne() {
		
		this.nom    = "";
		this.prenom = "";
		this.age    = 0;
	}
	
	
	public Personne(String pNom, String pPrenom, int pAge) {
		
		this.nom    = pNom;
		this.prenom = pPrenom;
		this.age    = pAge;
	}
	
	
	/**
	 * <b>COMPARER LA PERSONNE FOURNIE EN PARAMETRE AVEC L'OBJET COURANT (DE TYPE PERSONNE)</b>
	 * 
	 * @param pPersonne L'objet 'Personne' pass� en param�tre
	 * 
	 * @return   int<br/>
	 *            0  si (l'age de l'objet courant) = (l'age de l'objet pass� en param�tre).<br/>
	 *           -1  si (l'age de l'objet courant) < (l'age de l'objet pass� en param�tre).<br/>
	 *           +1  si (l'age de l'objet courant) > (l'age de l'objet pass� en param�tre).
	 */
	@Override
	public int compareTo(Personne pPersonne) {
		
		////////////////////////////////////////////////////////////////////////
		//(01.)DECLARER LA VARIABLE RETOUR DE CETTE METHODE
		////////////////////////////////////////////////////////////////////////
		int result = 0;
		
		////////////////////////////////////////////////////////////////////////
		//(02.)EFFECTUER UN COMPARAISON ENTRE LES 2 ELEMENTS SUIVANTS :
		//     -->L'OBJET FOURNI EN PARAMETRE (DE TYPE PERSONNE)
		//     -->L'OBJET COURANT (DE TYPE PERSONNE)
		////////////////////////////////////////////////////////////////////////
		 if (this.age < pPersonne.age) {
			System.out.println("(l'age de l'objet courant) < (l'age de l'objet pass� en param�tre)");
			result = -1;
			
		} else if (this.age > pPersonne.age) {
			System.out.println("(l'age de l'objet courant) > (l'age de l'objet pass� en param�tre)");
			result = 1;
			
		} else  {
			System.out.println("(l'age de l'objet courant) = (l'age de l'objet pass� en param�tre)");
			
			// TRAITER LE CAS OU LES 2 PERSONNES ONT LE MEME AGE 
			int isEqual = this.nom.compareTo(pPersonne.nom);
			
			// CAS N�1 : NOM DE LA PERSONNE COURANTE < NOM DE LA PERSONNE PASSEE EN PARAMETRE
			// CAS N�2 : NOM DE LA PERSONNE COURANTE > NOM DE LA PERSONNE PASSEE EN PARAMETRE
			// CAS N�3 : NOM DE LA PERSONNE COURANTE = NOM DE LA PERSONNE PASSEE EN PARAMETRE
			if      (isEqual < 0) { result = -1; } 
			else if (isEqual > 0) { result =  1; } 
			else                  { result =  0; }
		}
		return result;
	}
	

	@Override
	public String toString() {
		return "NOM : [" + this.nom + "] -- PRENOM : [" + this.prenom + "] -- AGE : [" + this.age + "]";
	}

	
	public String getNom   () { return nom;    }
	public String getPrenom() { return prenom; }
	public int    getAge   () { return age;    }

	public void setNom   (String pNom   ) { this.nom    = pNom;    }
	public void setPrenom(String pPrenom) { this.prenom = pPrenom; }
	public void setAge   (int    pAge   ) { this.age    = pAge;    }
}
