/**
 * 
 */
package user;

import java.util.ArrayList;
import java.util.List;

import domain.Personne;

/**
 * @author 1603599
 *
 */
public class PersonneUser {

	
	/**
	 * <b>CREER DES PERSONNES ET LES COMPARER</b>
	 */
	public void execute(){
		
		///////////////////////////////////////////////////////////////////////
		//(1.)CREER DES PERSONNES
		///////////////////////////////////////////////////////////////////////
		Personne personne01 = new Personne("Tcharou    ", "Dalgalian   ", 48);
		Personne personne02 = new Personne("Michel     ", "Gineste     ", 63);
		Personne personne03 = new Personne("Dylan      ", "Girard      ", 22);
		Personne personne04 = new Personne("William    ", "Hor         ", 26);
		Personne personne05 = new Personne("Hicham     ", "Timsit      ", 34);
		Personne personne06 = new Personne("Francesco  ", "Esposito    ", 34);
		Personne personne07 = new Personne("Jimmy      ", "Lefaix      ", 25);
		Personne personne08 = new Personne("Tony       ", "Ralaidovy   ", 32);
		Personne personne09 = new Personne("Gilles     ", "Brasseur    ", 29);
		Personne personne10 = new Personne("Jonathan   ", "Leconte     ", 28);
		Personne personne11 = new Personne("Jerome     ", "Millot      ", 34);
		Personne personne12 = new Personne("Hugo       ", "Hyolle      ", 30);
		Personne personne13 = new Personne("Roger      ", "Miot        ", 38);
		Personne personne14 = new Personne("Patrick    ", "Kabeya      ", 33);
		Personne personne15 = new Personne("Jean Claude", "Nsoki Miansi", 35);
		Personne personne16 = new Personne("John       ", "Holding     ", 31);
		Personne personne17 = new Personne("Intissar   ", "Mekenfes    ", 28);
		
		///////////////////////////////////////////////////////////////////////
		//(3.)CREER UNE LISTE
		///////////////////////////////////////////////////////////////////////
		List<Personne> personnes = new ArrayList<Personne>();
		
		///////////////////////////////////////////////////////////////////////
		//(4.)SAUVEGARDER TOUTES LES PERSONNES DANS LA LISTE
		///////////////////////////////////////////////////////////////////////
	    personnes.add(personne01);
	    personnes.add(personne02);
	    personnes.add(personne03);
	    personnes.add(personne04);
	    personnes.add(personne05);
	    personnes.add(personne06);
	    personnes.add(personne07);
	    personnes.add(personne08);
	    personnes.add(personne09);
	    personnes.add(personne10);
	    personnes.add(personne11);
	    personnes.add(personne12);
	    personnes.add(personne13);
	    personnes.add(personne14);
	    personnes.add(personne15);
	    personnes.add(personne16);
	    personnes.add(personne17);
	 	 
		///////////////////////////////////////////////////////////////////////
		//(5.)AFFICHER TOUTES LES PERSONNES DE LA LISTE
		///////////////////////////////////////////////////////////////////////
	  	System.out.println(" TAILLE DE LA LISTE :" + personnes.size());
	  	
    	System.out.println("+---------------------------------------------------+");
    	System.out.println("|          CONTENU DE LA LISTE                      |");
    	System.out.println("+-------------+-----------+------------+------------+");
    	System.out.println("| INDEX :     | NOM :     | PRENOM :   | AGE :      |");
    	System.out.println("+-------------+-----------+------------+------------+");

    	for (int cpt=0; cpt < personnes.size() ; cpt++ ) {
	    	
    	   	Personne currentPersonne  = personnes.get(cpt);
        	System.out.println("| " + cpt +    "        | " + currentPersonne.getNom()  
        											+  "| " + currentPersonne.getPrenom() 
        											+  "| " + currentPersonne.getAge() +     "|");
	    }
    	System.out.println("+---------------------------------------------------+");
	}

	
}
