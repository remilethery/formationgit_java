/**
 * 
 */
package starter;

import domain.Animal;
import domain.Chat;
import domain.Chien;
import domain.Crieur;
import domain.Mangeur;

/**
 * @author Tcharou
 *
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		////////////////////////////////////////////////////////////////////////////
		//(01.)CREATION D'OBJETS DE TYPE ENFANT 
		////////////////////////////////////////////////////////////////////////////
		Chien chien = new Chien();
		Chat chat = new Chat();
		
		////////////////////////////////////////////////////////////////////////////
		//(02.)TRANSTYPAGE DES OBJETS CREES 
		//     ->TYPES D'ORIGINE : LES TYPES ENFANTS 
		//     ->TYPES DE DESTINATION : LE TYPE 'Crieur'  
		////////////////////////////////////////////////////////////////////////////
		Crieur crieur01 = (Crieur)chien;
		Crieur crieur02 = (Crieur)chat;
		
		////////////////////////////////////////////////////////////////////////////
		//(03.)TRANSTYPAGE DES OBJETS CREES 
		//     ->TYPES D'ORIGINE : LES TYPES ENFANTS 
		//     ->TYPES DE DESTINATION : LE TYPE 'Mangeur' 
		////////////////////////////////////////////////////////////////////////////
		Mangeur mangeur01 = (Mangeur)chien;
		Mangeur mangeur02 = (Mangeur)chat;
		
		////////////////////////////////////////////////////////////////////////////
		//(04.)VERIFICATION DU TYPE REEL DES OBJETS CREES 
		////////////////////////////////////////////////////////////////////////////
		
		System.out.println("+--------------------------------------------------------------------+");
		System.out.println("| LE TYPE REEL DU CRIEUR-01                                          |");
		System.out.println("+-----------------+-----------------+----------------+---------------+");
		System.out.println("| TYPE 'Crieur' ? | TYPE 'Animal' ? | TYPE 'Chien' ? | TYPE 'Chat' ? |");
		System.out.println("+-----------------+-----------------+----------------+---------------+");
		System.out.println("| " + (crieur01 instanceof Crieur) + " | " + (crieur01 instanceof Animal) 
						+ " | " + (crieur01 instanceof Chien) + " | " + (crieur01 instanceof Chat) + " |");
		System.out.println("+-----------------+-----------------+----------------+---------------+");
		
		System.out.println("+--------------------------------------------------------------------+");
		System.out.println("| LE TYPE REEL DU CRIEUR-02                                          |");
		System.out.println("+-----------------+-----------------+----------------+---------------+");
		System.out.println("| TYPE 'Crieur' ? | TYPE 'Animal' ? | TYPE 'Chien' ? | TYPE 'Chat' ? |");
		System.out.println("+-----------------+-----------------+----------------+---------------+");
		System.out.println("| " + (crieur02 instanceof Crieur) + " | " + (crieur02 instanceof Animal) 
						+ " | " + (crieur02 instanceof Chien) + " | " + (crieur02 instanceof Chat) + " |");
		System.out.println("+-----------------+--------------------------------------------+");
		
		System.out.println("+---------------------------------------------------------------------+");
		System.out.println("| LE TYPE REEL DU MANGEUR-01                                          |");
		System.out.println("+------------------+-----------------+----------------+---------------+");
		System.out.println("| TYPE 'Mangeur' ? | TYPE 'Animal' ? | TYPE 'Chien' ? | TYPE 'Chat' ? |");
		System.out.println("+------------------+-----------------+----------------+---------------+");
		System.out.println("| " + (mangeur01 instanceof Mangeur) + " | " + (mangeur01 instanceof Animal) 
						+ " | " + (mangeur01 instanceof Chien) + " | " + (mangeur01 instanceof Chat) + " |");
		System.out.println("+------------------+-----------------+----------------+---------------+");

		System.out.println("+---------------------------------------------------------------------+");
		System.out.println("| LE TYPE REEL DU MANGEUR-02                                          |");
		System.out.println("+------------------+-----------------+----------------+---------------+");
		System.out.println("| TYPE 'Mangeur' ? | TYPE 'Animal' ? | TYPE 'Chien' ? | TYPE 'Chat' ? |");
		System.out.println("+------------------+-----------------+----------------+---------------+");
		System.out.println("| " + (mangeur02 instanceof Mangeur) + " | " + (mangeur02 instanceof Animal) 
						+ " | " + (mangeur02 instanceof Chien) + " | " + (mangeur02 instanceof Chat) + " |");
		System.out.println("+------------------+-----------------+----------------+---------------+");
		
		////////////////////////////////////////////////////////////////////////////
		//(05.)LANCEMENT DE LA METHODE 'crier' SUR LES OBJETS 'Crieur' 
		////////////////////////////////////////////////////////////////////////////
		crieur01.crier();
		crieur02.crier();

		////////////////////////////////////////////////////////////////////////////
		//(06.)LANCEMENT DE LA METHODE 'manger' SUR LES OBJETS 'Mangeur'  
		////////////////////////////////////////////////////////////////////////////
		mangeur01.manger();
		mangeur02.manger();
		
	}

}
