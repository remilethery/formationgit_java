/**
 * 
 */
package domain;

/**
 * @author Tcharou
 *
 */
public class Chat extends Animal {

	/**
	 * CONSTRUCTEUR SANS ARGUMENTS
	 */
	public Chat() {
		super();
	}

	/* (non-Javadoc)
	 * @see model.Animal#crier()
	 */
	@Override
	public void crier() {
		System.out.println("Miaou !!");
	}

	@Override
	public void manger() {

		System.out.println("Je mange des croquettes pour chat");
	};

}
