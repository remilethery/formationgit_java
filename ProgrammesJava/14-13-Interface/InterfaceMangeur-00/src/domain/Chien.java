/**
 * 
 */
package domain;

/**
 * @author Tcharou
 *
 */
public class Chien extends Animal {

	/**
	 * CONSTRUCTEUR SANS ARGUMENTS
	 */
	public Chien() {}

	/* (non-Javadoc)
	 * @see model.Animal#crier()
	 */
	@Override
	public void crier() {

		System.out.println("Ouaff !!");
	}

	@Override
	public void manger() {

		System.out.println("Je mange des croquettes pour chien");
	}

}
