/**
 * 
 */
package starter;

import domain.Animal;
import domain.Chat;
import domain.Chien;
import domain.Crieur;

/**
 * @author Tcharou
 *
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		////////////////////////////////////////////////////////////////////////////
		//(01.)CREATION D'OBJETS DE TYPE ENFANT 
		////////////////////////////////////////////////////////////////////////////
		Chien chien = new Chien();
		Chat chat = new Chat();
		
		////////////////////////////////////////////////////////////////////////////
		//(02.)TRANSTYPAGE DES OBJETS CREES 
		//     ->TYPES D'ORIGINE : LES TYPES ENFANTS 
		//     ->TYPES DE DESTINATION : LE TYPE PARENT  
		////////////////////////////////////////////////////////////////////////////
		Crieur crieur01 = (Crieur)chien;
		Crieur crieur02 = (Crieur)chat;
		
		////////////////////////////////////////////////////////////////////////////
		//(03.)VERIFICATION DU TYPE REEL DES OBJETS CREES 
		////////////////////////////////////////////////////////////////////////////
		System.out.println("+--------------------------------------------------------------------+");
		System.out.println("| LE TYPE REEL DE L'ANIMAL 01                                        |");
		System.out.println("+-----------------+-----------------+----------------+---------------+");
		System.out.println("| TYPE 'Crieur' ? | TYPE 'Animal' ? | TYPE 'Chien' ? | TYPE 'Chat' ? |");
		System.out.println("+-----------------+-----------------+----------------+---------------+");
		System.out.println("| " + (crieur01 instanceof Crieur) + " | " + (crieur01 instanceof Animal) + " | " + (crieur01 instanceof Chien) + " | " + (crieur01 instanceof Chat) + " |");
		System.out.println("+-----------------+-----------------+----------------+---------------+");
		
		System.out.println("+--------------------------------------------------------------------+");
		System.out.println("| LE TYPE REEL DE L'ANIMAL 02                                        |");
		System.out.println("+-----------------+-----------------+----------------+---------------+");
		System.out.println("| TYPE 'Crieur' ? | TYPE 'Animal' ? | TYPE 'Chien' ? | TYPE 'Chat' ? |");
		System.out.println("+-----------------+-----------------+----------------+---------------+");
		System.out.println("| " + (crieur02 instanceof Crieur) + " | " + (crieur02 instanceof Animal) + " | " + (crieur02 instanceof Chien) + " | " + (crieur02 instanceof Chat) + " |");
		System.out.println("+-----------------+--------------------------------------------+");
		
		////////////////////////////////////////////////////////////////////////////
		//(04.)LANCEMENT DE LA METHODE 'crier' SUR LES OBJETS CREES 
		////////////////////////////////////////////////////////////////////////////
		crieur01.crier();
		crieur02.crier();
		
	}

}
