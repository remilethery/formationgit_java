/**
 * 
 */
package domain;

/**
 * @author Tcharou
 *
 */
public enum ActionUtilisateur {
	
	///////////////////////////////////////////////////////////////////////////
	//(1.)OBJETS DIRECTEMENT CONSTRUITS
	///////////////////////////////////////////////////////////////////////////
	CREER             (1, "Cr�er un �tudiant"            ),
	RECHERCHER_PAR_ID (2, "Rechercher un �tudiant par id"),
	RECHERCHER_LISTE  (3, "Rechercher tous les �tudiants"),
	MODIFIER          (4, "Modifier un �tudiant"         ),
	SUPPRIMER         (5, "Supprimer un �tudiant"        ),
	QUITTER           (6, "Quitter l'application"        ),
	DEFAUT            (7, "Defaut"                       );
	
	/**
	 * <b>LE NUMERO DE L'ELEMENT ENUMERE</b>
	 */
	private int number = 0;
	   
	/**
	 * <b>LA VALEUR DE L'ELEMENT ENUMERE</b>
	 */
	private String value = null;
	   
	/**
	 * <b>CONSTRUCTEUR AVEC ARGUMENT</b>
	 */
	ActionUtilisateur(int pNumber, String pValue){
		this.number = pNumber;
		this.value = pValue;
	}
	   
	/**
	 * <b>RENVOYER LA VALEUR DE L'ELEMENT ENUMERE (SOUS FORME D'UNE CHAINE DE CARACTERES).</b>
	 * @return String La valeur de l'�l�ment �num�r� (sous forme d'une chaine de caract�res).
	 */
	@Override
	public String toString(){
		return "[" + this.number + "] -- [" + this.value + "]";
	}
}
