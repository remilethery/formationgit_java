/**
 * 
 */
package domain;

/**
 * @author Tcharou
 *
 */
public enum JourDeLaSemaine {
	
	///////////////////////////////////////////////////////////////////////////
	//(1.)OBJETS DIRECTEMENT CONSTRUITS
	///////////////////////////////////////////////////////////////////////////
	LUNDI    (1, "Lundi"   ),
	MARDI    (2, "Mardi"   ),
	MERCREDI (3, "Mercredi"),
	JEUDI    (4, "Jeudi"   ),
	VENDREDI (5, "Vendredi"),
	SAMEDI   (6, "Samedi"  ),
	DIMANCHE (7, "Dimanche");
	
	/**
	 * <b>LE NUMERO DE L'ELEMENT ENUMERE</b>
	 */
	private int number = 0;
	   
	/**
	 * <b>LA VALEUR DE L'ELEMENT ENUMERE</b>
	 */
	private String value = null;
	   
	/**
	 * <b>CONSTRUCTEUR AVEC ARGUMENT</b>
	 */
	JourDeLaSemaine(int pNumber, String pValue){
		this.number = pNumber;
		this.value = pValue;
	}
	   
	/**
	 * <b>RENVOYER LA VALEUR DE L'ELEMENT ENUMERE (SOUS FORME D'UNE CHAINE DE CARACTERES).</b>
	 * @return String La valeur de l'�l�ment �num�r� (sous forme d'une chaine de caract�res).
	 */
	@Override
	public String toString(){
		return this.number + " -- " + this.value;
	}
}
