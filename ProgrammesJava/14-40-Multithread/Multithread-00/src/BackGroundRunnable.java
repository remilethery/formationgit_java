
/**
 * <b>CLASSE QUI IMPLEMENTE UN TRAITEMENT D'ARRIERE-PLAN</b>
 * @author 1603599
 *
 */
public class BackGroundRunnable implements Runnable {

	
	   private Thread thread;
	   private String threadName;
	   
	   
	   /**
	    * CONSTRUCTEUR AVEC 1 ARGUMENT
	    * @param pThreadName Le nom du thread encapsule dans l'objet
	    */
	   public BackGroundRunnable( String pThreadName) {
	      System.out.println("CLASS : BackGroundRunnable -- METHOD : CONSTRUCTOR -- BEGIN");
	      this.threadName = pThreadName;
	      System.out.println("CLASS : BackGroundRunnable -- METHOD : CONSTRUCTOR -- END");
	   }
	   
	   /**
	    * METHODE QUI EFFECTUE UN TRAITEMENT D'ARRIERE-PLAN
	    */
	   @Override
	   public void run() {
	      System.out.println("CLASS : BackGroundRunnable -- METHOD : run -- BEGIN");
	      try {
	         for(int i = 4; i > 0; i--) {
	            System.out.println("Thread : [" + this.threadName + "], [" + i + "]");
	            // Let the thread sleep for a while.
	            Thread.sleep(500);
	         }
	      } catch (InterruptedException e) {
	         System.out.println("Thread interrupted : [" +  this.threadName + "]");
	      }
	      System.out.println("CLASS : BackGroundRunnable -- METHOD : run -- END");
	   }
	   
	   /**
	    * METHODE QUI EFFECTUE LES TACHES CI-DESSOUS :<br/>
	    *  ->CREER UN THREAD<br/> 
	    *  ->PASSER A CE THREAD UN OBJET QUI ENCAPSULE UNE METHODE<br/>
	    *  ->LANCER LE THREAD (CELA LANCE LA METHODE ENCAPSULEE DANS L'OBJET DE CE THREAD)<br/> 
	    */
	   public void start () {
	      System.out.println("Starting " +  threadName );
	      if (this.thread == null) {
	    	  this.thread = new Thread(this, threadName);
	    	  this.thread.start ();
	      }
	   }	
}
