/**
 * 
 */
package user;

import factory.AnimalFactory;
import model.Animal;

/**
 * @author 1603599
 *
 */
public class AnimalUser {

	
	private AnimalFactory animalFactory;
	
	/**
	 * <b>CONSTRUCTEUR</b>
	 * @param pName
	 * @return
	 */
	public AnimalUser() {
		
		this.animalFactory = new AnimalFactory();
	}
	
	/**
	 * <b>VERIFIER LE NOM D'ANIMAL FOURNI</b>
	 * @param pName
	 * @return
	 */
	public boolean checkAnimalName(String pName) {
		
		// DEMANDER A L'ANIMAL-FACTORY D'EFFECTUER LA VERIFICATION
		boolean resultOK = this.animalFactory.checkName(pName);
		return resultOK;
	}
	
	/**
	 * <b>TRAITEMENT PRINCIPAL DE L'ANIMAL-USER</b>
	 * <br/><b>FAIRE TRAVAILLER L'OBJET 'Animal'</b>
	 * @param pAnimalName
	 */
	public void execute (String pAnimalName) {
		 
		///////////////////////////////////////////////////////////////////////
		// 01.VERIFIER ET CONFIGURER L'ANIMAL-FACTORY
		///////////////////////////////////////////////////////////////////////
		this.animalFactory.configure(pAnimalName);
		
		///////////////////////////////////////////////////////////////////////
		// 02.DEMANDER UN OBJET A L'ANIMAL-FACTORY
		///////////////////////////////////////////////////////////////////////
		Animal animal = animalFactory.getObject();
		
		///////////////////////////////////////////////////////////////////////
		// 05.APPELER LA METHODE 'CRIER' SUR L'OBJET
		///////////////////////////////////////////////////////////////////////
		animal.crier();		
	}
	
}
