/**
 * 
 */
package main;

import user.PersonneUser;

/**
 * @author 1603599
 *
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		///////////////////////////////////////////////////////////////////////
		//1. CREER UN OBJET DE LA CLASSE PERSONNE USER
		///////////////////////////////////////////////////////////////////////
		PersonneUser personneUser = new PersonneUser();
		
		///////////////////////////////////////////////////////////////////////
		//2. APPELER LA METHODE EXECUTE DE L'OBJET CREE
		///////////////////////////////////////////////////////////////////////
		personneUser.execute();
	
	}
	
}
