/**
 * 
 */
package user;

import model.Personne;

/**
 * @author 1603599
 *
 */
public class PersonneUser {

	
	/**
	 * <b>CREER DES PERSONNES ET LES COMPARER</b>
	 */
	public void execute(){
		
		int resultat;
		
		///////////////////////////////////////////////////////////////////////
		//(1.)CREER UNE PERSONNE 1
		///////////////////////////////////////////////////////////////////////
		
		Personne personne1 = new Personne("Tcharou","Dalgalian", 48);
		
		///////////////////////////////////////////////////////////////////////
		//(2.)CREER UNE PERSONNE 2
		///////////////////////////////////////////////////////////////////////
		
		Personne personne2 = new Personne("Michel", "Gineste", 63);
		 
		///////////////////////////////////////////////////////////////////////
		//(3.)COMPARER LA PERSONNE 1 AVEC LA PERSONNE 2
		///////////////////////////////////////////////////////////////////////

		resultat = personne1.compareTo(personne2);
		System.out.println("Le resultat : " + resultat);
		
		///////////////////////////////////////////////////////////////////////
		//(4.)COMPARER LA PERSONNE 2 AVEC LA PERSONNE 1
		///////////////////////////////////////////////////////////////////////
		
		resultat = personne2.compareTo(personne1);
		System.out.println("Le resultat : " + resultat);
		
	}

	
}
