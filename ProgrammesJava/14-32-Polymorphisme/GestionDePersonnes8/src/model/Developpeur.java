package model;
/**
 * 
 */

/**
 * @author 1603599
 *
 */
public class Developpeur extends Employe {

	private String langage;
	private int experience;
	/**
	 * 
	 */
	public Developpeur() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param pNom
	 * @param pPrenom
	 * @param pPosteDeTravail
	 * @param pSalaire
	 */
	public Developpeur(String pNom, String pPrenom, String pPosteDeTravail, float pSalaire, String pLangage, int pExperience) {
		super(pNom, pPrenom, pPosteDeTravail, pSalaire);

		this.langage = pLangage;
		this.experience = pExperience;
	}
	
	public void initialiser() {

		this.langage = null;
		this.experience = 0;		
	}
	
	public void initialiser(String pLangage, int pExperience) {
		
		this.langage = pLangage;
		this.experience = pExperience;
	}
	
	public void afficher() {
		
		super.afficher();

		System.out.println("**********AFFICHAGE D'UN DEVELOPPEUR********************");
		System.out.println("Langage : " + this.langage);
		System.out.println("Experience : " + this.experience);
		System.out.println("****************************************************");
	}

}
