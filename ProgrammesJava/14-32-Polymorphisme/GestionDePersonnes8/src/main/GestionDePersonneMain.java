package main;

import model.Developpeur;

public class GestionDePersonneMain {

	public static void main(String[] args) {
		
		///////////////////////////////////////////////////////////////////////
		// CREER DES OBJETS DU TYPE 'PERSONNE'
		///////////////////////////////////////////////////////////////////////
		Developpeur developpeur = new Developpeur("Dalgalian", "Tcharou", "Developpeur", 3000, "java", 2);
		
		developpeur.initialiser();		
		developpeur.afficher();
		
		developpeur.initialiser("java", 2);		
		developpeur.afficher();
	}
	
}
