/**
 * 
 */
package main;

import user.AnimalUser;

/**
 * @author 1603599
 *
 */
public class Main{

	/**
	 * <b>CONSTRUCTEUR</b>
	 */
	public Main() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * <b>CONSTRUCTEUR</b>
	 * @param args
	 */
	public static void main(String[] args) {
		
		///////////////////////////////////////////////////////////////////////
		// 01.CREER UN ANIMAL-USER
		///////////////////////////////////////////////////////////////////////
		AnimalUser animalUser = new AnimalUser();
		
		///////////////////////////////////////////////////////////////////////
		// 02.APPELER LA METHODE 'EXECUTE' DE L'ANIMAL-USER
		///////////////////////////////////////////////////////////////////////
		animalUser.execute();
	}

}
