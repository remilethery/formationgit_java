/**
 * 
 */
package model;

/**
 * @author 1603599
 *
 */
public abstract class Animal {

	/**
	 * 
	 */
	public Animal() {
		// TODO Auto-generated constructor stub
	}
	
	public abstract void crier();
	public abstract void manger();
	public abstract void seDeplacer();

}
