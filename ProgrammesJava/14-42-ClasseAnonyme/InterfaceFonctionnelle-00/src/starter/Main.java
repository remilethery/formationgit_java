package starter;

import factory.Factory;
import model.Personne;
import parser.Parser;

/**
 * <b>CLASSE D'ENTREE DE L'APPLICATION</b><br/>
 * @author 1603599
 *
 */
public class Main {

	/**
	 * <b>METHODE D'ENTREE DE L'APPLICATION</b><br/>
	 * @author 1603599
	 *
	 */
	public static void main(String[] args) {
		
		///////////////////////////////////////////////////////////////////////////////
		// (00.)CREER UNE CHAINE A PARSER.
		///////////////////////////////////////////////////////////////////////////////
		String chaineAParser = "Manuel,DA ROCHA";
		
		///////////////////////////////////////////////////////////////////////////////
		// (01.)CREER UN OBJET DE TYPE 'Parser<Personne>'.
		///////////////////////////////////////////////////////////////////////////////
		Parser<Personne> personneParser = new Parser<Personne>();
		
		///////////////////////////////////////////////////////////////////////////////
		// (02.)CREER UN OBJET 'Factory<Personne>'.
		///////////////////////////////////////////////////////////////////////////////
		Factory<Personne> personneFactory = new Factory<Personne>() {
			
			@Override
			public Personne create(String pNom, String pPrenom) {
				
				System.out.println("INTERFACE : Factory<Personne> -- METHOD : create -- BEGIN");
				
				Personne personne = new Personne (pNom, pPrenom);
				
				System.out.println("INTERFACE : Factory<Personne> -- METHOD : create -- END");
				return personne;
			}
		};
		
		///////////////////////////////////////////////////////////////////////////////
		// (03.)SUR L'OBJET 'personneParser', DECLENCHER LA METHODE 'parse'.
		///////////////////////////////////////////////////////////////////////////////
		Personne personneCree = personneParser.parse(chaineAParser, personneFactory);
		
		///////////////////////////////////////////////////////////////////////////////
		// (04.)SUR L'OBJET 'personneCree', DECLENCHER LA METHODE 'parse'.
		///////////////////////////////////////////////////////////////////////////////
		System.out.println(personneCree.toString());
	}
}
