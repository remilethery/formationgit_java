package parser;

import factory.Factory;

/**
 * <b>INTERFACE DE PARSING</b><br/>
 *    ->DECOUPEUSE DE CHAINES DE CARACTERES.<br/>
 *    ->FABRIQUE D'OBJETS DE TYPE 'T'.<br/>
 *
 * @author 1603599
 */
public class Parser<T> {
	
	/**
	 * <b>METHODE DE PARSING</b><br/>
	 *    ->FABRIQUER UN OBJET DE TYPE 'T' A PARTIR D'UNE CHAINE DE CARACTERES.<br/>
	 *
	 * @author 1603599
	 * @param pDesignation
	 * @param pFactory
	 */
	public T parse(String pDesignation, Factory<T> pFactory) {
		
		System.out.println("CLASS : Parser<T> -- METHOD : parse -- BEGIN");

		///////////////////////////////////////////////////////////////////////////////
		// (01.)EFFECTUER LES 2 TACHES CI-DESSOUS :
		//      (01.01.)DECOUPER LA CHAINE DE CARACTERES.
		//      (01.02.)SAUVEGARDER LES CHAINES RESULTANT DU DECOUPAGE.
		///////////////////////////////////////////////////////////////////////////////
        String[] champs = pDesignation.split(",");
        String prenom = champs[0];
        String nom = champs[1];
        
		///////////////////////////////////////////////////////////////////////////////
		// (02.)FABRIQUER UN OBJET A PARTIR DES VARIABLES CREEES PRECEDEMMENT.
		///////////////////////////////////////////////////////////////////////////////
        T TCree = pFactory.create(nom, prenom);
        
		System.out.println("CLASS : Parser<T> -- METHOD : parse -- END");
        return TCree;
    }
}
