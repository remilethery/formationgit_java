package factory;

/**
 * <b>INTERFACE DE CREATION</b><br/>
 *    ->FABRIQUE D'OBJETS DE TYPE 'T'.<br/>
 * @author 1603599
 *
 */
//@FunctionalInterface
public interface Factory<T> {
	
	/**
	 * <b>METHODE DE CREATION</b><br/>
	 *    ->FABRIQUER UN OBJET DE TYPE 'T'.<br/>
	 *
	 * @author 1603599
	 * @param pNom
	 * @param pPrenom
	 */
	public abstract T create(String pNom, String pPrenom);
}
