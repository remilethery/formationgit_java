package starter;

import factory.Factory;
import factory.PersonneFactoryImpl;
import model.Personne;
import parser.Parser;

/**
 * <b>CLASSE D'ENTREE DE L'APPLICATION</b><br/>
 * @author 1603599
 *
 */
public class Main {

	/**
	 * <b>METHODE D'ENTREE DE L'APPLICATION</b><br/>
	 * @author 1603599
	 *
	 */
	public static void main(String[] args) {
		
		///////////////////////////////////////////////////////////////////////////////
		// (00.)CREER UNE CHAINE A PARSER.
		///////////////////////////////////////////////////////////////////////////////
		String chaineAParser = "Manuel,DA ROCHA";
		
		///////////////////////////////////////////////////////////////////////////////
		// (01.)CREER UN OBJET DE TYPE 'Parser<Personne>'.
		///////////////////////////////////////////////////////////////////////////////
		Parser<Personne> personneParser = new Parser<Personne>();
		
		///////////////////////////////////////////////////////////////////////////////
		// (02.)CREER UN OBJET DE TYPE 'Factory<Personne>'.
		///////////////////////////////////////////////////////////////////////////////
		Factory<Personne> personneFactory = new PersonneFactoryImpl();
		
		///////////////////////////////////////////////////////////////////////////////
		// (03.)SUR L'OBJET 'personneParser', DECLENCHER LA METHODE 'parse'.
		///////////////////////////////////////////////////////////////////////////////
		Personne personneCree01 = personneParser.parse(chaineAParser, personneFactory);
		Personne personneCree02 = personneParser.parse(chaineAParser, (nom, prenom)->{return new Personne(nom, prenom);});
		Personne personneCree03 = personneParser.parse(chaineAParser, (nom, prenom)->new Personne(nom, prenom));
		
		///////////////////////////////////////////////////////////////////////////////
		// (04.)SUR L'OBJET 'personneCree', DECLENCHER LA METHODE 'parse'.
		///////////////////////////////////////////////////////////////////////////////
		System.out.println(personneCree01.toString());
		System.out.println(personneCree02.toString());
		System.out.println(personneCree03.toString());
	}
}
