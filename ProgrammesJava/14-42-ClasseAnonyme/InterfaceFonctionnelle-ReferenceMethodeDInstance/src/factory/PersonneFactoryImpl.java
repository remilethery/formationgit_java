package factory;

import model.Personne;

/**
 * <b>INTERFACE DE CREATION</b><br/>
 *    ->FABRIQUE D'OBJETS DE TYPE 'T'.<br/>
 * @author 1603599
 *
 */
public class PersonneFactoryImpl implements Factory<Personne> {
	
	@Override
	public Personne create(String pNom, String pPrenom) {
		
		System.out.println("CLASS : PersonneFactoryImpl -- METHOD : create -- BEGIN");
		Personne personneCree = new Personne(pNom, pPrenom);
		System.out.println("CLASS : PersonneFactoryImpl -- METHOD : create -- END");
		return personneCree;
	}
}
