package model;

/**
 * <b>OBJET METIER</b><br/>
 *    ->TYPE : 'Personne'<br/>
 *
 * @author 1603599
 */
public class Personne {

	
	private String nom;
	private String prenom;
	
	
	/**
	 * <b>CONSTRUCTEUR AVEC ARGUMENTS</b><br/>
	 *    ->ARGUMENT : 'Personne'<br/>
	 *
	 * @author 1603599
	 */
	public Personne(String pNom, String pPrenom) {
		
		this.nom = pNom;
		this.prenom = pPrenom;
	}
	
	@Override
	public String toString() {
		
		String str = "Personne {"
										    + "nom : " + this.nom
								+ "," + " " + "prenom : " + this.prenom
								+ "}";
		return str;
	}


	public String getNom   () { return this.nom;    }
	public String getPrenom() { return this.prenom; }


	public void setNom   (String pNom   ) { this.nom    = pNom;    }
	public void setPrenom(String pPrenom) { this.prenom = pPrenom; }
}
