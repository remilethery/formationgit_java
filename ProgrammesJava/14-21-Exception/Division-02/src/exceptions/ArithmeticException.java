package exceptions;

public class ArithmeticException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * <b>MESSAGE DE L'EXCEPTION</b>
	 */
	private String message;
	
	
	/**
	 * <b>CONSTRUCTEUR AVEC 1 ARGUMENT</b>
	 */
	public ArithmeticException(String pMessage) {
		super();
		this.message = pMessage;
	}
	
	/**
	 * GETTER
	 */
	public String getMessage() {
		return this.message;
	}
}
