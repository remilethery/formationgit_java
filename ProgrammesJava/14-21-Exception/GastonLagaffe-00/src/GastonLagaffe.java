
public class GastonLagaffe {

	
	public void trierCourrierEnRetard(int pNombreLettres) {
		
		System.out.println("Quoi ? " + pNombreLettres + " lettres � trier ?");
		
		try {
			System.out.println("OK, OK, je vais m'y mettre ...");
			
			if (pNombreLettres > 2) {
				throw new Exception("Beaucoup trop de lettres ...");
			}
			System.out.println("Ouf, j'ai fini ...");
			
		} catch (Exception e) {
			System.out.println("M'enfin !..." + e.getMessage());
		}
		System.out.println("Apr�s tout ce travail, une sieste s'impose...");
	}
}
