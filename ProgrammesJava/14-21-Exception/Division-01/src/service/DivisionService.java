package service;

import java.util.Scanner;

/**
 * <b>SERVICE METIER RESPONSABLE DES TRAITEMENTS SUIVANTS :</b>
 * <b>LE SERVICE DE DIVISION DE NOMBRES</b>
 * 
 * @author Tcharou
 *
 */
public class DivisionService {
	
	/**
	 * <b>ATTRIBUT DE TYPE 'Scanner' EXISTANT EN UN SEUL EXEMPLAIRE DANS TOUTE L'APPLICATION.</b>
	 */
	private static Scanner scanner;
	
	/**
	 * <b>METHODE RESPONSABLE DES ETAPES SUIVANTES :</b>
	 * <b>-->(01.)EFFECTUER LA SAISIE DE DEUX NOMBRES DECIMAUX</b>
	 * <b>-->(02.)EFFECTUER LA DIVISION ENTRE LES DEUX NOMBRES SAISIS</b>
	 * 
	 */
	public void diviserDecimaux() {
	
		///////////////////////////////////////////////////////
		//(01.)CREER L'OBJET DE TYPE 'Scanner' 
		///////////////////////////////////////////////////////
		DivisionService.scanner = new Scanner(System.in);
		
		///////////////////////////////////////////////////////
		//(02.)SAISIR UN NOMBRE DECIMAL N�1
		///////////////////////////////////////////////////////
		System.out.println("Veuillez entrer un Nombre d�cimal N�1 :");
		float dividende = DivisionService.scanner.nextFloat();
		
		///////////////////////////////////////////////////////
		//(03.)SAISIR UN NOMBRE DECIMAL N�2
		///////////////////////////////////////////////////////
		System.out.println("Veuillez entrer un Nombre d�cimal N�2 :");
		float diviseur = DivisionService.scanner.nextFloat();

		///////////////////////////////////////////////////////
		//(04.)EFFECTUER LA DIVISION ENTRE LES 2 NOMBRE DECIMAUX SAISIS.
		///////////////////////////////////////////////////////
		float result = this.diviser(dividende, diviseur);
		
		///////////////////////////////////////////////////////
		//(05.)AFFICHER LE RESULTAT DE LA DIVISION EFFECTUEE.
		///////////////////////////////////////////////////////
		System.out.println("Le r�sultat de la division est :" + result);
		
		///////////////////////////////////////////////////////
		//(06.)FERMER L'OBJET DE TYPE 'Scanner' 
		///////////////////////////////////////////////////////
		DivisionService.scanner.close();
	}
	
	
	
	/**
	 * <b>METHODE RESPONSABLE DU TRAITEMENT SUIVANT :</b>
	 * <b>EFFECTUER LA DIVISION ENTRE DEUX NOMBRES DECIMAUX</b>
	 */
	private float diviser(float pDividende, float pDiviseur) throws ArithmeticException {
		
		///////////////////////////////////////////////////////
		//(01.)TRATITER LE CAS D'ERREUR : "DIVISEUR NULL"
		///////////////////////////////////////////////////////
		if (pDiviseur == 0) {
			throw new ArithmeticException("Le diviseur est null");
		} 
		///////////////////////////////////////////////////////
		//(02.)TRATITER LE CAS NOMINAL : "DIVISEUR NON NULL"
		///////////////////////////////////////////////////////
		float result = pDividende/pDiviseur;
		
		return result;
	}
	

}
