/**
 * 
 */
package domaine;

/**
 * @author Tcharou
 *
 */
public abstract class Animal {

	/**
	 * CONSTRUCTEUR SANS ARGUMENTS
	 */
	public Animal() {}
	
	public abstract void crier();

}
