/**
 * 
 */
package domaine;

/**
 * @author Tcharou
 *
 */
public class Chat extends Animal {

	/**
	 * CONSTRUCTEUR SANS ARGUMENTS
	 */
	public Chat() {
		super();
	}

	/* (non-Javadoc)
	 * @see model.Animal#crier()
	 */
	@Override
	public void crier() {
		System.out.println("Miaou !!");
	};

}
