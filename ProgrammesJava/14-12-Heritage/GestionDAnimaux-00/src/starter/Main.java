/**
 * 
 */
package starter;

import domaine.Animal;
import domaine.Chat;
import domaine.Chien;

/**
 * @author Tcharou
 *
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		////////////////////////////////////////////////////////////////////////////
		//(01.)CREATION D'OBJETS DE TYPE ENFANT 
		////////////////////////////////////////////////////////////////////////////
		Chien chien = new Chien();
		Chat chat = new Chat();
		
		////////////////////////////////////////////////////////////////////////////
		//(02.)TRANSTYPAGE DES OBJETS CREES 
		//     ->TYPES D'ORIGINE : LES TYPES ENFANTS 
		//     ->TYPES DE DESTINATION : LE TYPE PARENT  
		////////////////////////////////////////////////////////////////////////////
		Animal animal01 = (Animal)chien;
		Animal animal02 = (Animal)chat;
		
		////////////////////////////////////////////////////////////////////////////
		//(03.)VERIFICATION DU TYPE REEL DES OBJETS CREES 
		////////////////////////////////////////////////////////////////////////////
		System.out.println("+--------------------------------------------------------------+");
		System.out.println("| LE TYPE REEL DE L'ANIMAL 01                                  |");
		System.out.println("+-----------------+----------------+---------------------------+");
		System.out.println("| TYPE 'animal' ? | TYPE 'Chien' ? | TYPE 'Chat' ?             |");
		System.out.println("+-----------------+----------------+---------------------------+");
		System.out.println("| " + (animal01 instanceof Animal) + " | " + (animal01 instanceof Chien) + " | " + (animal01 instanceof Chat) + " |");
		System.out.println("+-----------------+----------------+---------------------------+");
		
		System.out.println("+--------------------------------------------------------------+");
		System.out.println("| LE TYPE REEL DE L'ANIMAL 02                                  |");
		System.out.println("+-----------------+----------------+---------------------------+");
		System.out.println("| TYPE 'animal' ? | TYPE 'Chien' ? | TYPE 'Chat' ?             |");
		System.out.println("+-----------------+----------------+---------------------------+");
		System.out.println("| " + (animal02 instanceof Animal) + " | " + (animal02 instanceof Chien) + " | " + (animal02 instanceof Chat) + " |");
		System.out.println("+-----------------+----------------+---------------------------+");
		
		////////////////////////////////////////////////////////////////////////////
		//(04.)LANCEMENT DE LA METHODE 'crier' SUR LES OBJETS CREES 
		////////////////////////////////////////////////////////////////////////////
		animal01.crier();
		animal02.crier();
		
	}

}
