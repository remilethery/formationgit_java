package starter;
import domain.Formateur;
import domain.Stagiaire;

public class Main {

	public static void main(String[] args) {

		
		///////////////////////////////////////////////////////////////////////
		// CREER DES OBJETS DU TYPE 'PERSONNE'
		///////////////////////////////////////////////////////////////////////
		
		Formateur formateurTcharou = new Formateur("Dalgalian", "Tcharou", 49, "Java", "Spring");
		formateurTcharou.afficher();

		
		Formateur formateurJeanJacques = new Formateur("Pagan", "Jean-Jacques", 43, "PHP", "WAMP");
		formateurJeanJacques.afficher();

		Stagiaire stagiaireMehdi = new Stagiaire("Salame", "Mehdi", 40, "XXX", "YYY");
		stagiaireMehdi.afficher();

		Stagiaire stagiaireManu = new Stagiaire("Da Rocha", "Manuel", 40, "XXX", "YYY");
		stagiaireManu.afficher();
		
	}
	
}
