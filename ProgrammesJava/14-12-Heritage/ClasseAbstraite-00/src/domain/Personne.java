package domain;

public abstract class Personne {

	
	///////////////////////////////////////////////////////////////////////////
	// 1. ATTRIBUTS DE LA CLASSE 
	///////////////////////////////////////////////////////////////////////////
	private String nom;
	private String prenom;
	private int age;
	
	///////////////////////////////////////////////////////////////////////////
	// 2. CONSTRUCTEURS DE LA CLASSE 
	///////////////////////////////////////////////////////////////////////////
	public Personne() {
		
		super();
	}

	/**
	 * @param nom Nom de la personne cr��e
	 * @param prenom Nom de la personne cr��e
	 * @param pAge Age de la personne cr��e
	 */
	public Personne(String pNom, String pPrenom, int pAge) {
		
		super();
		this.nom = pNom;
		this.prenom = pPrenom;
		this.age = pAge;
	}
	
	/**
	 * @param pNom Nom de la personne cr��e
	 * @param pPrenom Nom de la personne cr��e
	 */
	public Personne(String pNom, String pPrenom) {
		
		super();
		this.nom = pNom;
		this.prenom = pPrenom;
	}


	///////////////////////////////////////////////////////////////////////////
	// 3. METHODES DE LA CLASSE 
	///////////////////////////////////////////////////////////////////////////
	public abstract void afficher();
	
	///////////////////////////////////////////////////////////////////////////
	// 4. ACCESSEURS EN LECTURE DE LA CLASSE 
	///////////////////////////////////////////////////////////////////////////
	public String getNom   ()        { return this.nom;    }
	public String getPrenom()        { return this.prenom; }
	public int    getAge   ()        { return this.age;    }
	
	///////////////////////////////////////////////////////////////////////////
	// 5. ACCESSEURS EN ECRITURE DE LA CLASSE 
	///////////////////////////////////////////////////////////////////////////
	public void setNom   (String pNom   ) { this.nom    = pNom;    }
	public void setPrenom(String pPrenom) { this.prenom = pPrenom; }
	public void setAge   (int    pAge   ) { this.age    = pAge;    }
}
