package ui.controller;

import java.util.List;

/**
 * <b>INTERFACE DECLARANT LES OPERATIONS D'INTERFACE UTILISATEUR SUR L'ENTITE 'T'</b>
 * <b>INTERFACE GENERIQUE : ELLE S'APPLIQUE A L'ENTITE ABSTRAITE 'T'</b>
 * 
 * @author BOUHBEL     Azzedine, 
 * @author DA ROCHA    Manuel, 
 * @author DESCHAMPS   Francis, 
 * @author DORE        Emeryck, 
 * @author DURINGER    Gaspard, 
 * @author EL FATEOUI  Najim, 
 * @author FLAMAND     Kevin, 
 * @author GARNIER     Thomas, 
 * @author LAVIGNON    Baptiste, 
 * @author MARTIN      Guillaume, 
 * @author MASSE       Auriane, 
 * @author MOHAMED     Amin, 
 * @author PLOUCHARD   C�dric, 
 * @author RAJAOMAZAVA Dina, 
 * @author SALAME      Alexandre, 
 * @author SILOTIA     Gilles, 
 * @author VUILLAUME   Laureen, 
 *
 * @param <T>
 */
public interface IController<T> {

	/**
	 * <b>ENREGISTRER UNE ENTITE 'T'.</b>
	 * 
	 * @param pT L'entit� 'T' � enregistrer.
	 * @return   L'entit� 'T' enregistr�e.
	 */
	public abstract T enregistrer(T pT);
	
	/**
	 * <b>RECHERCHER UNE ENTITE 'T' PAR IDENTIFIANT.</b>
	 * 
	 * @param pId L'identifiant de l'entit� � rechercher.
	 * @return   L'entit� 'T' trouv�e.
	 */
	public abstract T rechercherParIdentifiant(long pId);
	
	/**
	 * <b>RECHERCHER TOUTES LES ENTITES 'T'.</b>
	 * 
	 * @return   La liste des entit�s trouv�es.
	 */
	public abstract List<T> rechercherListe();
	
	/**
	 * <b>MODIFIER UNE ENTITE 'T'.</b>
	 * 
	 * @param pT L'entit� 'T' � modifier.
	 * @return   L'entit� 'T' modifi�e.
	 */
	public abstract T modifier(T pT);
	
	/**
	 * <b>SUPPRIMER UNE ENTITE 'T'.</b>
	 * 
	 * @param pId L'identifiant de l'entit� � supprimer.
	 * @return   L'entit� 'T' supprim�e.
	 */
	public abstract T supprimer(long pId);
	
}
