package starter;

import persistence.entity.Personne;
import ui.controller.PersonneController;

public class Main {

	public static void main(String[] args) {

		/////////////////////////////////////////////////
		//(01.)CREER UN OBJET DE TYPE 'PERSONNE-UI'
		/////////////////////////////////////////////////
		PersonneController personneController = new PersonneController();
		
		/////////////////////////////////////////////////
		//(02.)CREER DES ENTITES DE TYPE 'PERSONNE'
		/////////////////////////////////////////////////
		//Personne personne01 = new Personne("Tcharou.Dalgalian@afpa.fr" , "Dalgalian", "Tcharou"     );
		//Personne personne02 = new Personne("Jean-jacques.Pagan@afpa.fr", "Pagan"    , "Jean-jacques");
		//Personne personne03 = new Personne("Amine.Bachri@afpa.fr"      , "Bachri"   , "Amine"       );
		
		/////////////////////////////////////////////////
		//(03.)ENREGISTRER LES ENTITES CREEES DANS L'INTERFACE UTILISATEUR
		/////////////////////////////////////////////////
		//personneController.enregistrer(personne01);
		//personneController.enregistrer(personne02);
		//personneController.enregistrer(personne03);
		
		/////////////////////////////////////////////////
		//(05.)CREER UNE ENTITE DE TYPE 'PERSONNE'
		/////////////////////////////////////////////////
		Personne personne04 = new Personne(1, "Tcharou.Dalgalian@afpa.frXXXX" , "DalgalianXXXX", "TcharouXXXX"     );
		
		/////////////////////////////////////////////////
		//(06.)MODIFIER L'ENTITES CREEE DANS L'INTERFACE UTILISATEUR
		/////////////////////////////////////////////////
		personneController.modifier(personne04);
	}
}
