package persistence.dao;

import java.util.List;

import business.exception.EntityAlreadyExistsException;
import business.exception.EntityNotFoundException;
import persistence.exception.DAOException;


/**
 * <b>INTERFACE DECLARANT LES OPERATIONS (PUBLIQUES) DE PERSISTANCE SUR L'ENTITE 'T'</b>
 * <br><b>INTERFACE GENERIQUE : ELLE S'APPLIQUE A L'ENTITE ABSTRAITE 'T'</b>
 * 
 * @author BOUHBEL     Azzedine, 
 * @author DA ROCHA    Manuel, 
 * @author DESCHAMPS   Francis, 
 * @author DORE        Emeryck, 
 * @author DURINGER    Gaspard, 
 * @author EL FATEOUI  Najim, 
 * @author FLAMAND     Kevin, 
 * @author GARNIER     Thomas, 
 * @author LAVIGNON    Baptiste, 
 * @author MARTIN      Guillaume, 
 * @author MASSE       Auriane, 
 * @author MOHAMED     Amin, 
 * @author PLOUCHARD   C�dric, 
 * @author RAJAOMAZAVA Dina, 
 * @author SALAME      Alexandre, 
 * @author SILOTIA     Gilles, 
 * @author VUILLAUME   Laureen, 
 *
 * @param <T>
 */
public interface IDAO<T> {
	
	/**
	 * <b>CREER UNE ENTITE 'T' DANS LES DONNEES PERSISTANTES</b>
	 * 
	 * @param pT L'entit� 'T' � cr�er.
	 * @return   L'entit� 'T' cr��e.
	 * 
	 * @throws EntityAlreadyExistsException Situation : L'objet existe d�j�.
	 * @throws EntityNotFoundException Situation : La r�cup�ration d'une cl� unique pour l'objet a �chou�.
	 * @throws DAOException Situation : Erreur technique dans le DAO.
	 */
	public abstract T create(T pT) throws EntityAlreadyExistsException, EntityNotFoundException, DAOException;
	
	/**
	 * <b>RECHERCHER UNE ENTITE 'T' DANS LES DONNEES PERSISTANTES</b>
	 * 
	 * @param pId L'identifiant de l'entit� 'T' � rechercher.
	 * @return    L'entit� 'T' trouv�e.
	 * 
	 * @throws EntityNotFoundException Situation : L'objet n'a pas �t� trouv�.
	 * @throws DAOException Situation : Erreur technique dans le DAO.
	 */
	public abstract T findById(long pId) throws EntityNotFoundException, DAOException;
	
	/**
	 * <b>RECHERCHER TOUTES LES ENTITES 'T' DANS LES DONNEES PERSISTANTES</b>
	 * 
	 * @return La liste des entit�s 'T' trouv�es.
	 * 
	 * @throws EntityNotFoundException Situation : Les objets n'ont pas �t� trouv�s.
	 * @throws DAOException Situation : Erreur technique dans le DAO.
	 */
	public abstract List<T> findList() throws DAOException;
	
	/**
	 * <b>MODIFIER UNE ENTITE 'T' DANS LES DONNEES PERSISTANTES</b>
	 * <br><b>(01.)Rechercher l'entit� 'T' (par son identifiant)</b>
	 * <br><b>(02.)Modifier l'entit� 'T' (Avec les attributs de l'entit� 'T' fournie)</b>
	 * 
	 * @param pT L'entit� 'T' � modifier.
	 * @return    L'entit� 'T' modifi�e.
	 * 
	 * @throws EntityNotFoundException Situation : L'objet n'a pas �t� trouv�.
	 * @throws DAOException Situation : Erreur technique dans le DAO.
	 */
	public abstract T updateById(T pT) throws EntityNotFoundException, DAOException;
	
	/**
	 * <b>SUPPRIMER UNE ENTITE 'T' DANS LES DONNEES PERSISTANTES</b>
	 * <br><b>(01.)Rechercher l'entit� 'T' (par son identifiant)</b>
	 * <br><b>(02.)Supprimer l'entit� 'T'</b>
	 * 
	 * @param pId L'identifiant de l'entit� 'T' � supprimer.
	 * @return    L'entit� 'T' supprim�e.
	 * 
	 * @throws EntityNotFoundException Situation : L'objet n'a pas �t� trouv�.
	 * @throws DAOException Situation : Erreur technique dans le DAO.
	 */
	public abstract T deleteById(long pId) throws EntityNotFoundException, DAOException;
}
