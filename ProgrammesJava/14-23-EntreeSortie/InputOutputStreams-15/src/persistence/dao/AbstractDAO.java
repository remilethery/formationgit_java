package persistence.dao;

import persistence.exception.DAOException;

/**
 * <b>CLASSE DECLARANT LES OPERATIONS (PROTEGEES) DE PERSISTANCE SUR L'ENTITE 'T'</b>
 * <br><b>CLASSE ABSTRAITE GENERIQUE : ELLE S'APPLIQUE A L'ENTITE ABSTRAITE 'T'</b>
 * 
 * @author BOUHBEL     Azzedine, 
 * @author DA ROCHA    Manuel, 
 * @author DESCHAMPS   Francis, 
 * @author DORE        Emeryck, 
 * @author DURINGER    Gaspard, 
 * @author EL FATEOUI  Najim, 
 * @author FLAMAND     Kevin, 
 * @author GARNIER     Thomas, 
 * @author LAVIGNON    Baptiste, 
 * @author MARTIN      Guillaume, 
 * @author MASSE       Auriane, 
 * @author MOHAMED     Amin, 
 * @author PLOUCHARD   C�dric, 
 * @author RAJAOMAZAVA Dina, 
 * @author SALAME      Alexandre, 
 * @author SILOTIA     Gilles, 
 * @author VUILLAUME   Laureen, 
 * 
 * @param <T>
 */
public abstract class AbstractDAO<T> implements IDAO<T> {

	/**
	 * <b>TRANSFORMATION D'UNE LIGNE DE TEXTE EN UNE ENTITE DE TYPE 'T'</b>
	 * <br><b>(01.)D�couper la ligne de texte (en fonction d'un s�parateur)</b>
	 * <br><b>(02.)Cr�er et alimenter un objet de type 'T' avec les champs issus du d�coupage.</b>
	 * 
	 * @param pLine La ligne de texte � transformer.
	 * @return L'entit� de type 'T' � cr�er. 
	 * 
	 * throws DAOException Situation : Erreur technique dans le DAO.
	 */
	protected abstract T transformLineToObject(String pLine) throws DAOException;
	
	/**
	 * <b>MODIFIER L'ENTITE DANS LES DONNEES PERSISTANTES, CONFORMEMENT AUX EXIGENCES SUIVANTES :</b>
	 * <br>(01.)Ecrire � partir de la position fournie en argument.
	 * <br>(02.)Ecrire la chaine de caract�res produite par l'objet fourni.
	 * <br>(03.)Ecrire un retour chariot apr�s la fin de la chaine de caract�res �crite.
	 * 
	 * @param pPosition La position � partir de laquelle l'�criture doit �tre effectu�e.
	 * @param pT La position L'entit� avec laquelle l'�criture doit �tre effectu�e.
	 * @return L'objet de type 'T' utilis�e pour cette op�ration. 
	 * 
	 * @throws DAOException Situation : Erreur technique dans le DAO.
	 */
	protected abstract T updateAtPosition(long pPosition, T pT) throws DAOException;
	
	/**
	 * <b>MODIFIER L'ENTITE DANS LES DONNEES PERSISTANTES, CONFORMEMENT AUX EXIGENCES SUIVANTES :</b>
	 * <br>(01.)Ecrire � partir de la position de fin du fichier.
	 * <br>(02.)Ecrire la chaine de caract�res produite par l'objet fourni.
	 * <br>(03.)Ecrire un retour chariot apr�s la fin de la chaine de caract�res �crite.
	 * 
	 * @param pPosition La position � partir de laquelle l'�criture doit �tre effectu�e.
	 * @return L'objet de type 'T' utilis�e pour cette op�ration. 
	 * 
	 * @throws DAOException Situation : Erreur technique dans le DAO.
	 */
	protected abstract T appendAtEOF(T pT) throws DAOException;
	
	/**
	 * <b>SUPPRIMER L'ENTITE DANS LES DONNEES PERSISTANTES, CONFORMEMENT AUX EXIGENCES SUIVANTES :</b>
	 * <br>(01.)D�but de l'op�ration de suppression : � la position fournie.
	 * <br>(02.)D�but de l'op�ration de suppression : au premier caract�re 'retour chariot' (inclus).
	 * 
	 * @param pPosition La position � partir de laquelle la suppression doit �tre effectu�e.
	 * @return La longueur de la ligne supprim�e dans cette op�ration. 
	 * 
	 * @throws DAOException Situation : Erreur technique dans le DAO.
	 */
	protected abstract long deleteLineFromLineBegin(long pPosition) throws DAOException;
}
