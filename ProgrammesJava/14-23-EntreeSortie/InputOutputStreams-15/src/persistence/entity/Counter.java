package persistence.entity;

/**
 * <b>DEFINITION DE L'ENTITE 'COUNTER'</b>
 * @author Tcharou
 *
 */
public class Counter {

	private long id;
	private String nom;
	private long valeur;
	
	/**
	 * <b>CONSTRUCTEUR SANS ARGUMENTS</b>
	 */
	public Counter() {}

	/**
	 * <b>CONSTRUCTEUR AVEC 2 ARGUMENTS</b>
	 */
	public Counter(String pNom, long pValeur) {
		this.nom = pNom;
		this.valeur = pValeur;
	}
	
	/**
	 * <b>CONSTRUCTEUR AVEC 3 ARGUMENTS</b>
	 */
	public Counter(long pId, String pNom, long pValeur) {
		this.id = pId;
		this.nom = pNom;
		this.valeur = pValeur;
	}
	
	/**
	 * <b>RENVOYER LE CONTENU DE L'ENTITE</b>
	 * <b>-->CE CONTENU DOIT �VOIR LE FORMAT SUIVANT : FORMAT 'CSV'</b>
	 */
	@Override
	public String toString() {
		String content = this.id + ";" + this.nom + ";" + this.valeur;
		return content;
	}

	public long   getId    () { return this.id;     }
	public String getNom   () { return this.nom;    }
	public long   getValeur() { return this.valeur; }
	
	public void setId    (long   pId    ) { this.id     = pId;     }
	public void setNom   (String pNom   ) { this.nom    = pNom;    }
	public void setValeur(long   pValeur) { this.valeur = pValeur; }
}
