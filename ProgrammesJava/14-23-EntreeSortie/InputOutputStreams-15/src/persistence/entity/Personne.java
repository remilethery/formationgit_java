package persistence.entity;

/**
 * <b>DEFINITION DE L'ENTITE 'PERSONNE'</b>
 * @author Tcharou
 *
 */
public class Personne {

	private long id;
	private String eMail;
	private String nom;
	private String prenom;
	
	/**
	 * <b>CONSTRUCTEUR SANS ARGUMENTS</b>
	 */
	public Personne() {}
	
	/**
	 * <b>CONSTRUCTEUR AVEC 3 ARGUMENTS</b>
	 */
	public Personne(String pEMail, String pNom, String pPrenom) {
		this.eMail  = pEMail;
		this.nom    = pNom;
		this.prenom = pPrenom;
	}

	/**
	 * <b>CONSTRUCTEUR AVEC 4 ARGUMENTS</b>
	 */
	public Personne(long pId, String pEMail, String pNom, String pPrenom) {
		this.id    = pId;
		this.eMail  = pEMail;
		this.nom    = pNom;
		this.prenom = pPrenom;
	}
	
	/**
	 * <b>RENVOYER LE CONTENU DE L'ENTITE</b>
	 * <b>-->CE CONTENU DOIT �VOIR LE FORMAT SUIVANT : FORMAT 'CSV'</b>
	 */
	@Override
	public String toString() {
		String content = this.id + ";" + this.eMail + ";" + this.nom + ";" + this.prenom;
		return content;
	}
	
	public long   getId    () { return this.id;     }
	public String getEMail () { return this.eMail;  }
	public String getNom   () { return this.nom;    }
	public String getPrenom() { return this.prenom; }

	public void setId    (long   pId    ) { this.id     = pId;     }
	public void setEMail (String pEMail ) { this.eMail  = pEMail;  }
	public void setNom   (String pNom   ) { this.nom    = pNom;    }
	public void setPrenom(String pPrenom) { this.prenom = pPrenom; }
}
