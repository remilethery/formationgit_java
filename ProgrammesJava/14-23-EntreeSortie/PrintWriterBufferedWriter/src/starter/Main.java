package starter;
import persistence.PrintWriterBufferedWriter;


/**
 * <b>CLASSE PRINCIPALE DE L'APPLICATION :</b><br>
 * 
 * @author Tcharou
 */
public class Main {
	
	public static void main(String[] args) {
		
		PrintWriterBufferedWriter printWriterBufferedWriter = new PrintWriterBufferedWriter();
		printWriterBufferedWriter.execute();
	}
}
