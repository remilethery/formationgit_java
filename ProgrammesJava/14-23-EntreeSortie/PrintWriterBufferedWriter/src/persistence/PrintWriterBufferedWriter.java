package persistence;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

/**
 * <b>CLASSE DEFINISSANT LES FONCTIONNALITES SUIVANTES :</b><br>
 *     <br>
 * <b>BUFFERED-WRITER :</b><br> 
 *     - STOCKER (DE FACON INTERMEDIAIRE) LES CARACTERES DANS UNE MEMOIRE-TAMPON<br>
 *     <br>
 * <b>OUTPUT-STREAM-WRITER :</b><br> 
 *     - TRANSFORMER LES CARACTERES EN OCTETS<br>
 *     - ECRIRE LES OCTETS DANS UN FICHIER<br>
 *     <br>
 * @author Tcharou
 */
public class PrintWriterBufferedWriter {

	
	private static final String PERSONNE_FILE = "data\\personne.csv";

	
	public void execute () {
		
		String personneLineTcharou = "1;Dalgalian;Tcharou;49";
		String personneLineJeanJacques = "2;Pagan;Jean-Jacques;43";
		String personneLineAmine = "3;Bachri;Amine;47";
		String personneLineMichel = "4;Benben;Michel;45";

        //char[] chars = content.toCharArray();

        try (PrintWriter printWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(PERSONNE_FILE))))) {

            // Write the string and change line
            printWriter.println(personneLineTcharou);
            printWriter.println(personneLineJeanJacques);
            printWriter.println(personneLineAmine);

            // Format the output
            printWriter.printf("%s\n",personneLineMichel);

        } catch (IOException e) {
            e.printStackTrace();
        }
	}
}
