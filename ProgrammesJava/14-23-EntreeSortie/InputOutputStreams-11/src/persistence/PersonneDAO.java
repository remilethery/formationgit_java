package persistence;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;


/**
 * <b>INTERFACE DECLARANT LES OPERATIONS DE PERSISTANCE SUR L'ENTITE 'PERSONNE'</b>
 * <b>INTERFACE NON GENERIQUE : ELLE S'APPLIQUE A L'ENTITE CONCRETE 'PERSONNE'</b>
 * 
 * @author BOUHBEL     Azzedine, 
 * @author DA ROCHA    Manuel, 
 * @author DESCHAMPS   Francis, 
 * @author DORE        Emeryck, 
 * @author DURINGER    Gaspard, 
 * @author EL FATEOUI  Najim, 
 * @author FLAMAND     Kevin, 
 * @author GARNIER     Thomas, 
 * @author LAVIGNON    Baptiste, 
 * @author MARTIN      Guillaume, 
 * @author MASSE       Auriane, 
 * @author MOHAMED     Amin, 
 * @author PLOUCHARD   C�dric, 
 * @author RAJAOMAZAVA Dina, 
 * @author SALAME      Alexandre, 
 * @author SILOTIA     Gilles, 
 * @author VUILLAUME   Laureen, 
 */
public class PersonneDAO {

	
	private static final String PERSONNE_FILE = "data\\personne.csv";
	private static final String COUNTER_FILE = "data\\counter.csv";

	
	/**
	 * <b>CREER UNE LIGNE CORRESPONDANT A UNE ENTITE 'PERSONNE' DANS LES DONNEES PERSISTANTES</b>
	 * @param pPersonneLine La ligne correspondant � l'entit� 'PERSONNE' � cr�er.
	 * @return   La ligne correspondant � l'entit� 'PERSONNE' cr��e.
	 */
	public String create (String pPersonneLine) {

        //char[] chars = content.toCharArray();

        try (PrintWriter printWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(PERSONNE_FILE, true))))) {

            // Write the string and change line
            printWriter.println(pPersonneLine);

            // Format the output
            //printWriter.printf("%s\n",pPersonneLine);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return pPersonneLine;
	}
	
	/**
	 * <b>RECUPERER LE DERNIER IDENTIFIANT INSERE DANS LES DONNEES PERSISTANTES 'PERSONNE'.</b>
	 * @return   Le dernier identifiant ins�r� dans le fichier 'PERSONNE'.
	 */
	public long getLastId() {
		
		long idFound = 0;
		
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(COUNTER_FILE)))) {

        	/////////////////////////////////////////////////////////
            // (01.)LIRE LA PREMIERE LIGNE DES DONNEES PERSISTANTES 'COUNTER'.
        	/////////////////////////////////////////////////////////
        	String idFoundStr = bufferedReader.readLine();
        	
        	/////////////////////////////////////////////////////////
        	// (02.)CONVERTIR L'IDENTIFIANT DE LA MANIERE SUIVANTE:
        	//      ->TYPE D'ORIGINE : String
        	//      ->TYPE DE DESTINATION : long
        	/////////////////////////////////////////////////////////
        	idFound = Long.parseLong(idFoundStr);

            // Format the input
            //bufferedReader.scanf("%s\n",pPersonneLine);

        } catch (IOException e) {
            e.printStackTrace();
        }
		System.out.println("Dernier identifiant trouv� : " + idFound);

        return idFound;
	}


}
