import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Main {

	public static void main(String[] args) {

		//////////////////////////////////////////////////////////////////
		//(01.)DECLARATION DES 2 FLUX SUIVANTS :
		//     ->UN FLUX DE LECTURE SUR UN FICHIER.  
		//     ->UN FLUX D'ECRICTURE SUR UN FICHIER.  
		//////////////////////////////////////////////////////////////////
		FileInputStream fis = null;
		FileOutputStream fos = null;

		//////////////////////////////////////////////////////////////////
		//(02.)LANCEMENT DES TRAITEMENTS DE LECTURE / ECRITURE :
		//////////////////////////////////////////////////////////////////
		try {
			//////////////////////////////////////////////////////////////////
			//(02.01.)CREATION DES 2 FLUX SUIVANTS:
			//        ->UN FLUX DE LECTURE SUR UN FICHIER.  
			//        ->UN FLUX D'ECRICTURE SUR UN FICHIER.  
			//////////////////////////////////////////////////////////////////
			fis = new FileInputStream(new File("data\\test01.txt"));
			fos = new FileOutputStream(new File("data\\test02.txt"));

			//////////////////////////////////////////////////////////////////
			// (02.02.)CREATION D'UN TABLEAU D'OCTETS DESTINE A L'USAGE SUIVANTS:
			//         -->STOCKER LES OCTETS ISSUS DES OPERATIONS DE LECTURE.
			//////////////////////////////////////////////////////////////////
			byte[] buffer = new byte[8];

			//////////////////////////////////////////////////////////////////
			// (02.03.)CREATION D'UNE VARIABLE STOCKANT LE RESULTAT DE LA LECTURE
			//////////////////////////////////////////////////////////////////
			int result = 0;

			//////////////////////////////////////////////////////////////////
			// (02.04.)OPERATIONS DE LECTURE REPETEES EN BOUCLE
			//////////////////////////////////////////////////////////////////
			/*
			while (fis.read(buffer) >= 0) {
				
				// On �crit dans notre deuxi�me fichier avec l'objet ad�quat
				fos.write(buffer);
				
				// On affiche ce qu'a lu notre boucle au format byte et au format char
				for (byte bit : buffer) {
					System.out.print("\t" + bit + "(" + (char) bit + ")");
				}
				System.out.println("");
				// Nous r�initialisons le buffer � vide
				// au cas o� les derniers byte lus ne soient pas un multiple de 8
				// Ceci permet d'avoir un buffer vierge � chaque lecture et ne pas avoir de
				// doublon en fin de fichier
				buffer = new byte[8];

			}*/
			do {
				result = fis.read(buffer);
				
				if (result >=0) {
					// On �crit dans notre deuxi�me fichier avec l'objet ad�quat
					fos.write(buffer);
					
					// On affiche ce qu'a lu notre boucle au format byte et au format char
					for (byte bit : buffer) {
						System.out.print("\t" + bit + "(" + (char) bit + ")");
					}
					System.out.println("");
					// Nous r�initialisons le buffer � vide
					// au cas o� les derniers byte lus ne soient pas un multiple de 8
					// Ceci permet d'avoir un buffer vierge � chaque lecture et ne pas avoir de
					// doublon en fin de fichier
					buffer = new byte[8];
				}
				
			} while (result >= 0);
			
			System.out.println("Copie termin�e !");

		} catch (FileNotFoundException e) {
			// Cette exception est lev�e si l'objet FileInputStream ne trouve
			// aucun fichier
			e.printStackTrace();
		} catch (IOException e) {
			// Celle-ci se produit lors d'une erreur d'�criture ou de lecture
			e.printStackTrace();
		} finally {
			// On ferme nos flux de donn�es dans un bloc finally pour s'assurer
			// que ces instructions seront ex�cut�es dans tous les cas m�me si
			// une exception est lev�e !
			try {
				if (fis != null)
					fis.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

			try {
				if (fos != null)
					fos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
