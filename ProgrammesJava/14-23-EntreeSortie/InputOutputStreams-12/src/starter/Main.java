package starter;
import persistence.dao.PersonneDAO;


/**
 * <b>CLASSE PRINCIPALE DE L'APPLICATION :</b><br>
 * 
 * @author Tcharou
 */
public class Main {
	
	public static void main(String[] args) {
		
		///////////////////////////////////////////////////
		//(01.)CREER UN OBJET D'ACCES AUX DONNEES PERSISTANTES 'PERSONNE'
		///////////////////////////////////////////////////
		PersonneDAO personneDAO = new PersonneDAO();

		///////////////////////////////////////////////////
		//(01.)RECUPERER LE DERNIER IDENTIFIANT INSERE DANS LES DONNEES PERSISTANTES 'PERSONNE'.
		///////////////////////////////////////////////////
		long lastId = personneDAO.getLastId();

		///////////////////////////////////////////////////
		//(02.)CREER DES PERSONNES
		///////////////////////////////////////////////////
		String personneLineTcharou     = lastId + 1 + ";" + "Dalgalian;Tcharou;49";
		String personneLineJeanJacques = lastId + 2 + ";" + "Pagan;Jean-Jacques;43";
		String personneLineAmine       = lastId + 3 + ";" + "Bachri;Amine;47";
		String personneLineMichel      = lastId + 4 + ";" + "Benben;Michel;45";

		///////////////////////////////////////////////////
		//(03.)INSERER LES PERSONNES DANS LES DONNEES PERSISTANTES
		///////////////////////////////////////////////////
		personneDAO.create(personneLineTcharou);
		personneDAO.create(personneLineJeanJacques);
		personneDAO.create(personneLineAmine);
		personneDAO.create(personneLineMichel);
	}
}
