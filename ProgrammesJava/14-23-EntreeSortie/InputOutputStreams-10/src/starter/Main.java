package starter;
import persistence.PersonneDAO;


/**
 * <b>CLASSE PRINCIPALE DE L'APPLICATION :</b><br>
 * 
 * @author Tcharou
 */
public class Main {
	
	public static void main(String[] args) {
		
		///////////////////////////////////////////////////
		//(01.)CREER DES PERSONNES
		///////////////////////////////////////////////////
		String personneLineTcharou = "1;Dalgalian;Tcharou;49";
		String personneLineJeanJacques = "2;Pagan;Jean-Jacques;43";
		String personneLineAmine = "3;Bachri;Amine;47";
		String personneLineMichel = "4;Benben;Michel;45";
		
		///////////////////////////////////////////////////
		//(02.)INSERER LES PERSONNES DANS LES DONNEES PERSISTANTES
		///////////////////////////////////////////////////
		PersonneDAO personneDAO = new PersonneDAO();
		personneDAO.create(personneLineTcharou);
		personneDAO.create(personneLineJeanJacques);
		personneDAO.create(personneLineAmine);
		personneDAO.create(personneLineMichel);
	}
}
