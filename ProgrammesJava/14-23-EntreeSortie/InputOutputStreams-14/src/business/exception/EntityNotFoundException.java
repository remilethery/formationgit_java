package business.exception;

/**
 * <b>EXCEPTION LANCEE LORS D'UNE OPERATION DE PERSISTANCE</b>
 * <b>DESCRIPTION : SITUATION DANS LAQUELLE L'ENTITE N'A PAS ETE TROUVEE</b>
 * @author Tcharou
 */
public class EntityNotFoundException extends Exception {
	
	/**
	 * <b>DEFAULT SERIAL VERSION ID</b>
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * <b>CONSTRUCTEUR AVEC 1 ARGUMENT</b>
	 */
	public EntityNotFoundException(String pMessage) {
		super(pMessage);
	}
}
