package business.exception;

/**
 * <b>EXCEPTION LANCEE LORS D'UNE OPERATION DE PERSISTANCE</b>
 * <b>DESCRIPTION : SITUATION DANS LAQUELLE L'ENTITE EXISTE DEJA</b>
 * @author Tcharou
 */
public class EntityAlreadyExistsException extends Exception {
	
	/**
	 * <b>DEFAULT SERIAL VERSION ID</b>
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * <b>CONSTRUCTEUR AVEC 1 ARGUMENT</b>
	 */
	public EntityAlreadyExistsException(String pMessage) {
		super(pMessage);
	}

}
