package persistence.dao;

import java.util.List;

import business.exception.EntityAlreadyExistsException;
import business.exception.EntityNotFoundException;


/**
 * <b>INTERFACE DECLARANT LES OPERATIONS DE PERSISTANCE SUR L'ENTITE 'T'</b>
 * <b>INTERFACE GENERIQUE : ELLE S'APPLIQUE A L'ENTITE ABSTRAITE 'T'</b>
 * 
 * @author BOUHBEL     Azzedine, 
 * @author DA ROCHA    Manuel, 
 * @author DESCHAMPS   Francis, 
 * @author DORE        Emeryck, 
 * @author DURINGER    Gaspard, 
 * @author EL FATEOUI  Najim, 
 * @author FLAMAND     Kevin, 
 * @author GARNIER     Thomas, 
 * @author LAVIGNON    Baptiste, 
 * @author MARTIN      Guillaume, 
 * @author MASSE       Auriane, 
 * @author MOHAMED     Amin, 
 * @author PLOUCHARD   C�dric, 
 * @author RAJAOMAZAVA Dina, 
 * @author SALAME      Alexandre, 
 * @author SILOTIA     Gilles, 
 * @author VUILLAUME   Laureen, 
 *
 * @param <T>
 */
public interface IDAO<T> {
	
	/**
	 * <b>CREER UNE ENTITE 'T' DANS LES DONNEES PERSISTANTES</b>
	 * @param pT L'entit� 'T' � cr�er.
	 * @return   L'entit� 'T' cr��e.
	 */
	public abstract T create(T pT) throws EntityAlreadyExistsException;
	
	/**
	 * <b>RECHERCHER UNE ENTITE 'T' DANS LES DONNEES PERSISTANTES</b>
	 * @param pId L'identifiant de l'entit� 'T' � rechercher.
	 * @return    L'entit� 'T' trouv�e.
	 */
	public abstract T findById(long pId) throws EntityNotFoundException;
	
	/**
	 * <b>RECHERCHER TOUTES LES ENTITES 'T' DANS LES DONNEES PERSISTANTES</b>
	 * @return La liste des entit�s 'T' trouv�es.
	 */
	public abstract List<T> findList();
	
	/**
	 * <b>MODIFIER UNE ENTITE 'T' DANS LES DONNEES PERSISTANTES</b>
	 * @param pT L'entit� 'T' � modifier.
	 * @return    L'entit� 'T' modifi�e.
	 */
	public abstract T update(T pT) throws EntityNotFoundException;
	
	/**
	 * <b>SUPPRIMER UNE ENTITE 'T' DANS LES DONNEES PERSISTANTES</b>
	 * @param pT L'entit� 'T' � supprimer.
	 * @return    L'entit� 'T' supprim�e.
	 */
	public abstract T delete(long pId) throws EntityNotFoundException;
	

}
