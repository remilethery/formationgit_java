package ui.controller;

import java.util.List;

import business.exception.EntityAlreadyExistsException;
import business.exception.EntityNotFoundException;
import persistence.dao.PersonneDAO;
import persistence.entity.Personne;

/**
 * <b>CLASSE DECLARANT LES OPERATIONS D'INTERFACE UTILISATEUR SUR L'ENTITE 'PERSONNE'</b>
 * <b>INTERFACE NON GENERIQUE : ELLE S'APPLIQUE A L'ENTITE CONCRETE 'PERSONNE'</b>
 * 
 * @author BOUHBEL     Azzedine, 
 * @author DA ROCHA    Manuel, 
 * @author DESCHAMPS   Francis, 
 * @author DORE        Emeryck, 
 * @author DURINGER    Gaspard, 
 * @author EL FATEOUI  Najim, 
 * @author FLAMAND     Kevin, 
 * @author GARNIER     Thomas, 
 * @author LAVIGNON    Baptiste, 
 * @author MARTIN      Guillaume, 
 * @author MASSE       Auriane, 
 * @author MOHAMED     Amin, 
 * @author PLOUCHARD   C�dric, 
 * @author RAJAOMAZAVA Dina, 
 * @author SALAME      Alexandre, 
 * @author SILOTIA     Gilles, 
 * @author VUILLAUME   Laureen, 
 */
public class PersonneController implements IController<Personne>{

	
	private PersonneDAO personneDAO;
	
	
	/**
	 * <b>CONSTRUCTEUR SANS ARGUMENT</b>
	 */
	public PersonneController () {
		this.personneDAO = new PersonneDAO();
	}
	
	@Override
	public Personne enregistrer(Personne pPersonne) {
		
		Personne personneCreated = null;
		try {
			personneCreated = this.personneDAO.create(pPersonne);
			
		} catch (EntityAlreadyExistsException e) {
			System.out.println("Erreur -- " + e.getMessage());
		}
		return personneCreated;
	}

	@Override
	public Personne rechercherParIdentifiant(long pId) {
		
		Personne personneFound = null;
		
		try {
			personneFound = this.personneDAO.findById(pId);
			
		} catch (EntityNotFoundException e) {
			System.out.println("Erreur -- " + e.getMessage());
		}
		return personneFound;
	}

	@Override
	public List<Personne> rechercherListe() {
		
		List<Personne> personnesFound = this.personneDAO.findList();
		
		return personnesFound;
	}

	@Override
	public Personne modifier(Personne pPersonne) {
		
		Personne personneUpdated = null;
		
		try {
			personneUpdated = this.personneDAO.update(pPersonne);
			
		} catch (EntityNotFoundException e) {
			System.out.println("Erreur -- " + e.getMessage());
		}
		return personneUpdated;
	}

	@Override
	public Personne supprimer(long pId) {
		
		Personne personneDeleted = null;
		
		try {
			personneDeleted = this.personneDAO.delete(pId);
			
		} catch (EntityNotFoundException e) {
			System.out.println("Erreur -- " + e.getMessage());
		}
		return personneDeleted;
	}
}
